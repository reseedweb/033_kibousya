<section id="slider">
	<div class="slider-content wrapper clearfix">
			<ul class="bxslider">
				<li>
					<a href="<?php bloginfo('url'); ?>/basic">
						<img src="<?php bloginfo('template_url'); ?>/img/top/slider_main_img1.jpg" alt="top" />										
					</a>
				</li>
				<li>
					<a href="<?php bloginfo('url'); ?>/about">
						<img src="<?php bloginfo('template_url'); ?>/img/top/slider_main_img2.jpg" alt="top" />					
					</a>
				</li>
				<li>
					<a href="<?php bloginfo('url'); ?>/bridal">
						<img src="<?php bloginfo('template_url'); ?>/img/top/slider_main_img3.jpg" alt="top" />					
					</a>
				</li>
				<li>
					<a href="<?php bloginfo('url'); ?>/factory">
						<img src="<?php bloginfo('template_url'); ?>/img/top/slider_main_img4.jpg" alt="top" />					
					</a>
				</li>
			</ul>

			<div id="bx-pager">
				<a data-slide-index="0" href="<?php bloginfo('url'); ?>/basic">
					<img src="<?php bloginfo('template_url'); ?>/img/top/slider_thumb_img1.jpg" alt="top" />					
				</a>
				<a data-slide-index="1" href="<?php bloginfo('url'); ?>/about">
					<img src="<?php bloginfo('template_url'); ?>/img/top/slider_thumb_img2.jpg" alt="top" />			
				</a>
				<a data-slide-index="2" href="<?php bloginfo('url'); ?>/bridal">
					<img src="<?php bloginfo('template_url'); ?>/img/top/slider_thumb_img3.jpg" alt="top" />					
				</a>
				<a data-slide-index="3" href="<?php bloginfo('url'); ?>/factory">
					<img src="<?php bloginfo('template_url'); ?>/img/top/slider_thumb_img4.jpg" alt="top" />					
				</a>
			</div>		
		</div>
</section>



		