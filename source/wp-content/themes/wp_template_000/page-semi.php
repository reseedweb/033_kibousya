<?php get_header(); ?>	
	<?php get_template_part('part','breadcrumb'); ?>
	<div class="main-content clearfix"><!-- begin main-content -->
		<h1 class="basic-top-title">	
			セミオーダー紙袋
		</h1><!-- ./basic-top-title -->
		
		<div class="message-left message-200 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/semi_content_img.jpg" alt="semi" />
			</div><!-- ./image -->
			<div class="text">
				<div class="msg200-title semi-clr">
					<p class="msg200-title-first">「納期を急いでいる」というお客様には</p>
					<p>セミオーダー紙袋がオススメです。</p>
				</div><!-- ./msg200-title -->
				<div class="msg200-text">
					国内にて紙袋を制作しますので納期を早くすることができ、お客様のオリジナル紙袋制作の納期に間に合わせることができます。以下の仕様項目からそれぞれ仕様を選んで頂いた後、セミオーダー紙袋のお見積りフォームよりご依頼ください。
				</div><!-- ./msg200-text -->
			</div><!-- ./text -->
		</div><!-- end message-280 -->

		<?php get_template_part('part','order'); ?>
		
	</div><!-- end main-content -->	
	<?php get_template_part('part','contact'); ?>	
<?php get_footer(); ?>