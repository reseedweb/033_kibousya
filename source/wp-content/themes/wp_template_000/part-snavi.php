<div class="sidebar-row clearfix"><!-- begin sidebar-row -->
	<aside id="sideNavi" class="sideNavi-content"><!-- begin sideNavi -->
		<h2 class="title">ご利用ガイド</h2>
		<ul>
			<li><a href="<?php bloginfo('url'); ?>/about">初めての方へ<i class="fa fa-play"></i></a></li>
			<li><a href="<?php bloginfo('url'); ?>/flow">ご注文の流れ<i class="fa fa-play"></i></a></li>
			<li><a href="<?php bloginfo('url'); ?>/draft">データ入稿について<i class="fa fa-play"></i></a></li>
			<li><a href="<?php bloginfo('url'); ?>/faq">よくあるご質問<i class="fa fa-play"></i></a></li>
			<li><a href="<?php bloginfo('url'); ?>/factory">工場紹介<i class="fa fa-play"></i></a></li>
			<li><a href="<?php bloginfo('url'); ?>/company">会社案内<i class="fa fa-play"></i></a></li>
			<li><a href="<?php bloginfo('url'); ?>/privacy">プライバシーポリシー<i class="fa fa-play"></i></a></li>
			<li><a href="<?php bloginfo('url'); ?>/contact">お問い合わせ<i class="fa fa-play"></i></a></li>
		</ul>
	</aside><!-- end sideNavi -->
</div><!-- end sidebar-row -->