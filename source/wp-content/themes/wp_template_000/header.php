<!DOCTYPE html>
<htmlz>
    <head>
        <!-- meta -->        
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />         
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- title -->
        <title><?php wp_title( '|', true); ?></title>
        <meta name="robots" content="noindex,follow,noodp" />
        
        <link rel="profile" href="http://gmpg.org/xfn/11" />                
        
        <!-- global javascript variable -->
        <script type="text/javascript">
            var CONTAINER_WIDTH = '1090px';
            var CONTENT_WIDTH = '1060px';
            var BASE_URL = '<?php bloginfo('url'); ?>';
            var TEMPLATE_URI = '<?php bloginfo('template_url') ?>';
            var CURRENT_MODULE_URI = '';
            Date.now = Date.now || function() { return +new Date; };            
        </script>        
        <!-- Bootstrap -->
        <link href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css" rel="stylesheet" />
        <link href="<?php bloginfo('template_url'); ?>/css/bootstrap-theme.min.css" rel="stylesheet" />   
        <!-- fontawesome -->
        <link href="<?php bloginfo('template_url'); ?>/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="<?php bloginfo('template_url'); ?>/js/html5shiv.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/respond.min.js"></script>
        <![endif]-->        
        
        <script src="<?php bloginfo('template_url'); ?>/js/jquery.js" type="text/javascript"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/jquery.plugins.js" type="text/javascript"></script>               
        <script src="<?php bloginfo('template_url'); ?>/js/bootstrap.min.js" type="text/javascript"></script>        

        <link href="<?php bloginfo('template_url'); ?>/style.css?<?php echo md5(date('l jS \of F Y h:i:s A')); ?>" rel="stylesheet" />
        <script src="<?php bloginfo('template_url'); ?>/js/config.js" type="text/javascript"></script>        		
        <?php wp_head(); ?>
    </head>
    <body>     
        <div id="screen_type"></div>
        
        <div id="wrapper"><!-- begin wrapper -->

            <section id="top"><!-- begin top -->                			
				<div class="top_content wrapper clearfix">
					<h1 class="top-text">オリジナル紙袋の印刷・制作ならお任せ！お気軽にお問い合わせ下さい。</h1>
					<ul class="top-links">
						<li><i class="fa fa-caret-right"></i><a href="<?php bloginfo('url'); ?>/about">初めての方へ</a></li>
						<li><i class="fa fa-caret-right"></i></i><a href="<?php bloginfo('url'); ?>/draft">データ入稿について</a></li>
						<li><i class="fa fa-caret-right"></i></i><a href="<?php bloginfo('url'); ?>/privacy">プライバシーポリシー</a></li>                        
					</ul>
				</div><!-- ./top_content -->  			
			</section><!-- end top -->          

            <header><!-- begin header -->			
				<div class="header-content wrapper clearfix">            
					<div class="logo">
						<a href="<?php bloginfo('url'); ?>/">
							<img alt="logo" src="<?php bloginfo('template_url'); ?>/img/common/logo.jpg" />
						</a>
					</div>
					<div class="header-info">
						<div class="header-tel">
							<img alt="tel" src="<?php bloginfo('template_url'); ?>/img/common/header_tel.jpg" />
						</div>
						<div class="header-con">
							<a href="<?php bloginfo('url'); ?>/contact">
								<img alt="con" src="<?php bloginfo('template_url'); ?>/img/common/header_con.jpg" />
							</a>
						</div>
					</div>					
				</div><!-- ./header-content -->	                                			
			</header><!-- end header -->
			
			<?php get_template_part('part','gnavi'); ?>
			           
			<?php if(is_home()) : ?>
                <?php get_template_part('part','slider'); ?>
				<?php get_template_part('part','maintop'); ?>
			<?php endif; ?> 
			<section id="page-feature">              	
				<?php if(is_page('basic')) : ?>
					<div class="page-feature-content wrapper">
						<img src="<?php bloginfo('template_url'); ?>/img/content/basic_content_top_img.jpg" alt="page feature" />                       												
					</div>		
				<?php elseif(is_page('semi')) : ?>
					<div class="page-feature-content wrapper">
						<img src="<?php bloginfo('template_url'); ?>/img/content/semi_content_top_img.jpg" alt="page feature" />                       												
					</div>
				<?php elseif(is_page('full')) : ?>
					<div class="page-feature-content wrapper">
						<img src="<?php bloginfo('template_url'); ?>/img/content/full_content_top_img.jpg" alt="page feature" />                       												
					</div>
				<?php elseif(is_page('bridal')) : ?>
					<div class="page-feature-content wrapper">
						<img src="<?php bloginfo('template_url'); ?>/img/content/bridal_content_top_img.jpg" alt="page feature" />                       												
					</div>
				<?php else :?>
				<?php endif; ?> 						
            </section>           			
                    						            
            <section id="content"><!-- begin content -->
                <div class="wrapper two-cols-left clearfix"><!-- begin two-cols -->
                    <main class="primary"><!-- begin primary -->
       