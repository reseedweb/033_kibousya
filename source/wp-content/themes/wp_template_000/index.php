<?php get_header(); ?>

<div class="primary-row clearfix"><!-- begin primary-row -->    
	<div class="top-title clearfix">
		<h2 class="h2-title">紙袋製作サイトOnly Oneが選ばれる理由</h2>    
		<h2 class="top-title-en">Why Choose</h2>		
	</div>	
	<div class="top-content-info1"><!-- begin top-content-info1 -->
		<div class="message-group message-col230 clearfix"><!-- begin message-group -->
			<div class="message-row clearfix"><!--message-row -->
				<div class="message-col">					
					<div class="top-box-image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/top_content_bimg1.jpg" alt="top" />
					</div><!-- end image -->    
					<div class="top-box1-title">
						オリジナル紙袋が1枚81円〜
						最短1週間で制作できる！
					</div>
					<div class="top-box1-text">
						テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト
					</div>            
				</div><!-- end message-col -->
				
				<div class="message-col">					
					<div class="top-box-image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/top_content_bimg2.jpg" alt="top" />
					</div><!-- end image -->    
					<div class="top-box1-title">
						ブライダル紙袋ならお任せ
						ワンランク上のクオリティ！
					</div>
					<div class="top-box1-text">
						テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト
					</div>            
				</div><!-- end message-col -->
				
				<div class="message-col">					
					<div class="top-box-image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/top_content_bimg3.jpg" alt="top" />
					</div><!-- end image -->    
					<div class="top-box1-title">
						自社工場ならではの対応力
						制作実績は10,000件以上！
					</div>
					<div class="top-box1-text">
						テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト
					</div>            
				</div><!-- end message-col -->          
			</div><!-- end message-row -->
		</div><!-- end message-group -->
	</div><!-- end top-content-info1 -->
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
    <div class="top-title clearfix">
		<h2 class="h2-title">初めてご利用される方へ</h2>    
		<h2 class="top-title-en">For Beginner</h2>		
	</div>	
    <div class="top-content-info2 clearfix">
		<div class="info2-text1">
			初めてご利用される方へ
		</div>
		<div class="info2-text2">
			<p>紙袋制作Only Oneは紙袋の印刷・制作の専門会社です。</p>
			<p class="pt10">初めてご利用されるお客様をご案内させていただきます。</p>
		</div>
		<div class="info2-text3">
			<div class="info2-listtext1">
				もっと安く作りたい！				
			</div>
			<div class="info2-listtext2">
				入稿方法が分からない…
			</div>
		</div>
		<div class="info2-text4 info2-text3">
			<div class="info2-listtext1">
				急ぎの案件で困っている…				
			</div>
			<div class="info2-listtext2">
				様々な要望に応えてほしい！
			</div>
		</div>
		<div class="info2-btn">
			<a href="<?php bloginfo('url'); ?>/about">
				<img src="<?php bloginfo('template_url'); ?>/img/content/top_about_btn.jpg" alt="top" />
			</a>
		</div>
    </div><!-- end top-content-info2 -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->    
		<div class="top-content-info3 clearfix">
			<div class="info3-text1">
				<span class="info3-ftext1">格安＆短納期</span>で制作されたい方へ
			</div>
			<div class="info3-text2">
				箔押し・名入れ印刷
			</div>		
			<div class="info3-text3">
				<span class="info3-icon">●</span>キラキラした箔押し印刷<br />
				<span class="info3-icon">●</span>ロゴマークや名前だけの名入れ印刷
			</div>
			<div class="info3-btn">
				<a href="<?php bloginfo('url'); ?>/basic">
					<img src="<?php bloginfo('template_url'); ?>/img/content/top_basic_btn.jpg" alt="top" />
				</a>
			</div>
		</div><!-- end top-content-info3 -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->    
		<div class="top-content-info4 clearfix">
			<div class="info4-text1">
				オリジナル<br />
				ブライダル紙袋
			</div>		
			<div class="info4-text2">
				一生に一度の記念に♪
			</div>
			<div class="info4-text3">
				<span class="info4-icon">●</span>自分だけのオリジナルデザインが作れます！<br />
				<span class="info4-icon">●</span>高級感のあるワンランク上の作品が作れます！
			</div>
			<div class="info4-btn">
				<a href="<?php bloginfo('url'); ?>/bridal">
					<img src="<?php bloginfo('template_url'); ?>/img/content/top_bridal_btn.jpg" alt="top" />
				</a>
			</div>
		</div><!-- end top-content-info4 -->
	</div><!-- end primary-row -->	
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
    <div class="top-title clearfix">
		<h2 class="h2-title">工場紹介</h2>    
		<h2 class="top-title-en">Plant Introduction</h2>		
	</div>	
	<div class="top-content-info5 clearfix">
		<div class="info5-text1">
			弊社保有の紙袋生産工場の様子と<br />
			機械設備をご紹介いたします！
		</div>		
		<div class="info5-btn">
			<a href="<?php bloginfo('url'); ?>/factory">
				<img src="<?php bloginfo('template_url'); ?>/img/content/top_factory_btn.jpg" alt="top" />
			</a>
		</div>
	</div><!-- end top-content-info4 -->
</div><!-- end primary-row -->

<?php get_template_part('part','blog'); ?>

<?php get_template_part('part','contact'); ?>

<div class="primary-row clearfix"><!-- begin primary-row -->
    <div class="top-title clearfix">
		<h2 class="h2-title">紙袋制作Only Oneからのメッセージ</h2>    
		<h2 class="top-title-en">Message</h2>		
	</div>	   
    <div class="message-left message-275 clearfix">
        <div class="image">
            <img src="<?php bloginfo('template_url'); ?>/img/content/top_content_img.jpg" alt="top" />
        </div>
        <div class="text">
			テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト
        </div>
    </div><!-- end message-275 -->
</div><!-- end primary-row -->

<?php get_footer(); ?>