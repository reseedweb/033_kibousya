<?php get_header(); ?>	
	<?php get_template_part('part','breadcrumb'); ?>	
	<div class="main-content clearfix"><!-- begin main-content -->
		<h1 class="basic-top-title">	
			会社案内
		</h1><!-- ./basic-top-title -->
		<p>紙袋制作サイト「Only One」を運営している会社についてご紹介いたします。</p>		
		<div class="mt20 clearfix">
			<h2 class="factory-title">会社案内</h2>		
			<table class="companty-table">
				<tr>
					<th>会社名</th>
					<td>株式会社ウィル</td>
				</tr>
				<tr>
					<th>代表取締役</th>
					<td>越智季夫</td>
				</tr>
				<tr>
					<th>資本金</th>
					<td>1000万円</td>
				</tr>
				<tr>
					<th>電話</th>
					<td>06-6321-8111</td>
				</tr>
				<tr>
					<th>FAX</th>
					<td>06-6815-1115</td>
				</tr>
				<tr>
					<th>所在地</th>
					<td>大阪市東淀川区瑞光2丁目13番31号</td>
				</tr>
				<tr>
					<th>事業内容</th>
					<td>
						1、紙加工製品の企画、立案<br />
						2、不動産の賃貸及び管理<br />
						3、紙加工製品及び文化用品の輸入販売<br />
						4、前各号に附帯関連する一切の業務
					</td>
				</tr>

			</table>
		</div>	
	</div><!-- end main-content -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->	
		<h2 class="factory-title">アクセス </h2>
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3278.1797253808877!2d135.539961!3d34.75106520000001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6000e3ebe32ec1a9%3A0xe9b3c2e1db34090c!2z44CSNTMzLTAwMDUg5aSn6Ziq5bqc5aSn6Ziq5biC5p2x5reA5bed5Yy655Ge5YWJ77yS5LiB55uu77yR77yT4oiS77yT77yR!5e0!3m2!1sja!2sjp!4v1424856379016" width="760" height="320" frameborder="0" style="border:0"></iframe>		
	</div><!-- end primary-row -->	
		
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="factory-title">紙袋制作Only Oneからのメッセージ</h2>		   
		<div class="message-left message-275 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/company_content_img.jpg" alt="company" />
			</div>
			<div class="text">
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト
			</div>
		</div><!-- end message-275 -->
	</div><!-- end primary-row -->
	<?php get_template_part('part','contact'); ?>	
<?php get_footer(); ?>