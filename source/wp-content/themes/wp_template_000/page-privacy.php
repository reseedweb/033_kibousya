<?php get_header(); ?>	
	<?php get_template_part('part','breadcrumb'); ?>	
	<div class="main-content clearfix"><!-- begin main-content -->
		<h1 class="basic-top-title">	
			プライバシーポリシー
		</h1><!-- ./basic-top-title -->
		<p>紙袋制作サイト「Only One」では、ではご提供頂いた個人情報の保護について最大限の注意を払っております。</p>		
		<div class="mt20 clearfix">
			<h2 class="factory-title">1. 個人情報保護</h2>		
			<div class="privacy-text">
				<p>・弊社では会員様により登録された個人及び団体や法人の情報については、紙袋制作サイト「Only One」 において最<br /><span class="pravicy-line">先端の機能やサービスを開発・提供するためにのみ利用し、会員個人情報の保護に細心の注意を払うものとします。</span></p>				
				<p>・このプライバシーポリシーの適用範囲は、紙袋制作サイト「Only One」で提供されるサービスのみであります。</p>
				<p><span class="pravicy-line">(範囲は下記、第1項に規定)</span></p>
				<p>・本規約に明記された場合を除き、目的以外の利用は致しません。(目的は下記、第2項に規定)<br />
				   ・本規約に明記された場合を除き、第三者への開示は致しません。(管理は下記、第2項に規定)</p>
				<p>・その他本規約に規定された方法での適切な管理を定期的に行います。</p>
				<p>・紙袋制作サイト「Only One」は利用者の許可なくして、プライバシーポリシーの変更をすることができます。紙袋</span><br />
				   <span class="pravicy-line">制作サイト「Only One」が、個人情報取得内容の変更・利用方法の変更・開示内容の変更等をした際には、利用者</span><br />
				   <span class="pravicy-line">がその内容を知ることができるよう、弊社ホームページのお知らせに公開し、このプライバシーポリシーに反映する</span>
				   <span class="pravicy-line">ことにより通知致します。
				</p>
			</div><!-- end privacy-text -->
		</div>				
	</div><!-- end main-content -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->	
		<h2 class="factory-title">2. 紙袋制作サイト「Only One」の考え方が適用される範囲</h2>		
		<div class="privacy-text">
			<p>・紙袋制作サイト「Only One」のプライバシーポリシーについての考え方は、会員が紙袋制作サイト「Only One」</p>			 
			<p><span class="pravicy-line">のサービスを利用される場合に適用されます。</span></p>
			<p>・会員が紙袋制作サイト「Only One」のサービスを利用される際に収集される個人情報は、 紙袋制作サイト「Only <br /><span class="pravicy-line">One」の個人情報保護についての考え方に従って管理されます。</span></p>
			<p>・紙袋制作サイト「Only One」の個人情報保護考え方は、紙袋制作サイト「Only One」が直接提供されるサービス<br /><span class="pravicy-line">のみであり、リンク等でつながった他の組織・会社等のサービスは適用範囲外となります。</span></p>
			<p>・紙袋制作サイト「Only One」のサービスのご利用は、利用者の責任において行われるものとします。</p>
			<p>・弊社のホームページ及び当ホームページにリンクが設定されている他のホームページから取得された各種情報の利<br /><span class="pravicy-line">用によって生じたあらゆる損害に関して、紙袋制作サイト「Only One」は一切の責任を負いません。</span></p>
		</div><!-- end privacy-text -->
	</div><!-- end primary-row -->	
	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->	
		<h2 class="factory-title">3. 紙袋制作サイト「Only One」の個人情報の収集と利用</h2>		
		<div class="privacy-text">
			<p>・紙袋制作サイト「Only One」のプライバシーポリシーについての考え方は、会員が紙袋制作サイト「Only One」<br /><span class="pravicy-line">のサービスを利用される場合に適用されます。</span></p>
			<p>・会員が紙袋制作サイト「Only One」のサービスを利用される際に収集される個人情報は、紙袋制作サイト「Only<br /><span class="pravicy-line">One」の個人情報保護についての考え方に従って管理されます。</span></p>
			<p>・紙袋制作サイト「Only One」の個人情報保護考え方は、 紙袋制作サイト「Only One」が直接提供されるサービス<br /><span class="pravicy-line">のみであり、リンク等でつながった他の組織・会社等のサービスは適用範囲外となります。</span></p>
			<p>・紙袋制作サイト「Only One」のサービスのご利用は、利用者の責任において行われるものとします。</p>
			<p>・弊社のホームページ及び当ホームページにリンクが設定されている他のホームページから取得された各種情報の利<br /><span class="pravicy-line">用によって生じたあらゆる損害に関して、紙袋制作サイト「Only One」は一切の責任を負いません。</span></p>
		</div><!-- end privacy-text -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->	
		<h2 class="factory-title">4. 問い合わせ先</h2>		
		<div class="privacy-text">
			<p>・ここに示した個人情報についての考え方についてご不明な点などございましたら、次のアドレスまで電子メールでお<br /><span class="pravicy-line">問い合わせください。個人情報管理担当 : info@kibousya.net</span></p>   
		</div><!-- end privacy-text -->
	</div><!-- end primary-row -->
	
	<?php get_template_part('part','contact'); ?>	
<?php get_footer(); ?> 