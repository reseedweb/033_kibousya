<?php get_header(); ?>	
	<?php get_template_part('part','breadcrumb'); ?>
	<div class="main-content clearfix"><!-- begin main-content -->
		<h1 class="basic-top-title">	
			ベーシック紙袋
		</h1><!-- ./basic-top-title -->
		
		<div class="message-left message-200 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/basic_content_img.jpg" alt="basic" />
			</div><!-- ./image -->
			<div class="text">
				<div class="msg200-title basic-clr">
					<p class="msg200-title-first">「コストをできるだけ抑えたい」というお客様には</p>
					<p>ベーシック紙袋がオススメです。</p>
				</div><!-- ./msg200-title -->
				<div class="msg200-text">
					箔押し・名入れ印刷で紙袋を制作して頂くことにより、オリジナル紙袋制作の価格を抑えることができます。以下の仕様項目からそれぞれ仕様を選んで頂いた後、ベーシック紙袋のお見積りフォームよりご依頼ください。
				</div><!-- ./msg200-text -->
			</div><!-- ./text -->
		</div><!-- end message-280 -->

		<?php get_template_part('part','order'); ?>
		
	</div><!-- end main-content -->	
	<?php get_template_part('part','contact'); ?>	
<?php get_footer(); ?>