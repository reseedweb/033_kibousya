<div class="primary-row clearfix"><!-- begin primary-row -->
    <div class="top-title clearfix">
		<h2 class="h2-title">ご注文の流れ</h2>    
		<h2 class="top-title-en">Flow</h2>		
	</div>	
	<div class="top-part-contact clearfix">
		<div class="part-contact-text1">
			お問い合わせから納品までに流れ
		</div>		
		<div class="part-contact-flow">
			<a href="<?php bloginfo('url'); ?>/flow">
				<img src="<?php bloginfo('template_url'); ?>/img/content/top_part_flow.jpg" alt="top" />
			</a>
		</div>
		<ul class="part-contact-flist clearfix">
			<li>お問い合わせ</li>
			<li>お見積もりのご提示</li>
			<li>ご発注データ入稿</li>
			<li>海外の製造工場にて制作・検品</li>
			<li>完成した製品の輸入・通関業務</li>
			<li>商品の発送</li>
		</ul>
		<div class="part-contact-bg">
			<div class="part-contact-btn">
				<a href="<?php bloginfo('url'); ?>/contact">
					<img src="<?php bloginfo('template_url'); ?>/img/content/top_part_con_btn.jpg" alt="top" />
				</a>
			</div>
		</div>
	</div><!-- end top-part-contact -->
</div><!-- end primary-row -->