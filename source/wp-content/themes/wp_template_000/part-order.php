		<div class="part-basic-box"><!-- begin part-basic-box -->
			<div class="primary-row clearfix">
				<div class="part-basic-btn htabs clearfix">
					<a href="#tab1" class="basic-tab"><span>仕様・制作ガイド</span></a>
					<a href="#tab2" class="basic-tab" id="tab-form"><span>ハンドルの素材を選ぶ</span></a>					
				</div>	
			</div>
			<div class="primary-box clearfix">
				<div id="tab1">
					<div class="primary-row clearfix">
						<h2 class="basic-tab-title">ご注文の流れ</h2>
						<div class="basic-tab-text">	
							<p>ブライダル紙袋のお見積もりフォームです。必要事項をご記入の上、送信ボタンを押して下さい。</p>
							<p>スタッフがメールを確認後、ご対応させていただきます。</p>							
						</div>						
						<p class="pt20"><img src="<?php bloginfo('template_url'); ?>/img/content/basic_content_step.jpg" alt="basic" /></p>
						<h3 class="basic-title clearfix"><span class="basic-tstep">Step.1</span><span class="basic-ttext">袋の素材</span></h3>
						<p class="pb20">4タイプの袋の中から、オリジナルの紙袋の袋の素材をお選びください。</p>
						<div class="message-left message-160 clearfix">
							<div class="image">
								<img src="<?php bloginfo('template_url'); ?>/img/content/basic_material01.jpg" alt="basic" />
							</div><!-- ./image -->
							<div class="text">
								<h4 class="msg160-title">
									コ`ート紙
								</h4><!-- ./msg200-title -->
								<div class="msg160-table">
									印刷を美しくするために、紙の表面にラミネート加工処理を施した素材です。高級感のある光沢が特徴で、アパレル・ブライダル系の紙袋によく使用されます。箔押し・名入れなどを入れて頂くのがオススメです。
								</div><!-- ./msg200-text -->
								<table class="basic-table">
									<tr>
										<th>140g</th>
										<td>一般的な紙袋の厚さです。</td>
									</tr>
									<tr>
										<th>157g</th>
										<td>140ｇより厚めで、少し高級感を出したい方にオススメです。</td>
									</tr>
									<tr>
										<th>190g</th>
										<td>高級感のある厚さ。エンボスを掛けることでさらに高級感が増します。</td>
									</tr>
								</table>
							</div><!-- ./text -->
						</div><!-- end message-160 -->
						
						<div class="message-left message-160 basic-box clearfix">
							<div class="image">
								<img src="<?php bloginfo('template_url'); ?>/img/content/basic_material02.jpg" alt="basic" />
							</div><!-- ./image -->
							<div class="text">
								<h4 class="msg160-title">
									未晒クラフト紙
								</h4><!-- ./msg200-title -->
								<div class="msg160-table">
									茶色のクラフト紙で再生紙の様な風合いがあり、エコなイメージの袋で使われます。ベースがシンプルなだけに、デザインでアレンジできる範囲も広く、食料品のお店から、雑貨店、アパレルショップまで様々なお店で愛用されています。
								</div><!-- ./msg200-text -->
								<table class="basic-table">
									<tr>
										<th>120g</th>
										<td>お客様からよく注文される、一般的な未晒クラフト紙の厚さです。</td>
									</tr>									
								</table>
							</div><!-- ./text -->
						</div><!-- end message-160 -->
						
						<div class="message-left message-160 basic-box clearfix">
							<div class="image">
								<img src="<?php bloginfo('template_url'); ?>/img/content/basic_material03.jpg" alt="basic" />
							</div><!-- ./image -->
							<div class="text">
								<h4 class="msg160-title">
									晒クラフト紙
								</h4><!-- ./msg200-title -->
								<div class="msg160-table">
									<p>お菓子などお持ち帰り用のショッピングバッグとして使われます。</p>
									<p>強度があり、ラミネートなしでも使えます。</p>
									<p>オフセット印刷との印刷適性はあまり良くありません。</p>
								</div><!-- ./msg200-text -->
								<table class="basic-table">
									<tr>
										<th>100g</th>
										<td>お菓子などお持ち帰り用のショッピングバッグとして使われます。</td>										
									</tr>									
									<tr>
										<th>120g</th>
										<td>強度があり、ラミネートなしでも使えます。オフセット印刷の印刷適性はあまり良くありません。</td>
									</tr>
								</table>
							</div><!-- ./text -->
						</div><!-- end message-160 -->						
											
						<div class="message-left message-160 basic-box clearfix">
							<div class="image">
								<img src="<?php bloginfo('template_url'); ?>/img/content/basic_material04.jpg" alt="basic" />
							</div><!-- ./image -->
							<div class="text">
								<h4 class="msg160-title">
									カラー用紙
								</h4><!-- ./msg200-title -->
								<div class="msg160-table">
									<p>コート紙にカラーのフィルムを貼り合せた用紙です。18色から選べます。</p>									
								</div><!-- ./msg200-text -->
								<table class="basic-table">	
									<tr>
										<th>140g</th>
										<td>140gのコート紙にカラーのフィルムを貼り合せた用紙。18色から選べます。</td>
									</tr>
									<tr>
										<th>157g</th>
										<td>157gのコート紙にカラーのフィルムを貼り合せた用紙。18色から選べます。</td>
									</tr>
									<tr>
										<th>190g</th>
										<td>190gのコート紙にカラーのフィルムを貼り合せた用紙。18色から選べます。</td>
									</tr>
									<tr>
										<th>E190g</th>
										<td>190gのコート紙にカラーのフィルムを貼り合せ、その上にエンボスを掛け た最高級な用紙です。8色から選べます。</td>
									</tr>
								</table>
							</div><!-- ./text -->
						</div><!-- end message-160 -->

						<h3 class="basic-title clearfix"><span class="basic-tstep">Step.2</span><span class="basic-ttext">サイズを選ぶ</span></h3>
						<p>10タイプのサイズの中から、ご用途に合わせて紙袋のサイズをお選びください。以下の規格以外のサイズでも対応可能ですので、お気軽にお問い合わせください。</p>
						<div class="primary-row clearfix">
							<div class="message-left message-160 clearfix">
								<div class="image">
									<img src="<?php bloginfo('template_url'); ?>/img/content/basic_size01.jpg" alt="basic" />
								</div><!-- ./image -->
								<div class="text">
									<h4 class="msg160-title">
										Ｂ４Ｌ（縦）サイズ
									</h4><!-- ./msg200-title -->
									<h5 class="msg160-size-title"><span class="msg160-icon">●</span>巾000 × 高さ000 × マチ000mm</h5>
									<p>アクセサリーなどのちょっとした小物に最適なサイズです。小さいサイズのバッグを取り揃えることで、バッグを渡す方の細やかな心遣いが伝わります。</p>
									<p>A4サイズがピッタリ入ります。</p>
								</div><!-- ./text -->
							</div><!-- end message-160 -->
						</div><!-- ./primary-row-->		
						
						<div class="primary-row clearfix">
							<div class="message-left message-160 clearfix">
								<div class="image">
									<img src="<?php bloginfo('template_url'); ?>/img/content/basic_size02.jpg" alt="basic" />
								</div><!-- ./image -->
								<div class="text">
									<h4 class="msg160-title">
										Ｂ４Ｍ（縦）サイズ
									</h4><!-- ./msg200-title -->
									<h5 class="msg160-size-title"><span class="msg160-icon">●</span>巾000 × 高さ000 × マチ000mm</h5>
									<p>雑貨や食料品の店舗などでよく使われる一般的なサイズです。オプションなどでアクセントを付けると、店舗の印象を際立たせることができます。</p>
								</div><!-- ./text -->
							</div><!-- end message-160 -->
						</div><!-- ./primary-row-->	

						<div class="primary-row clearfix">
							<div class="message-left message-160 clearfix">
								<div class="image">
									<img src="<?php bloginfo('template_url'); ?>/img/content/basic_size03.jpg" alt="basic" />
								</div><!-- ./image -->
								<div class="text">
									<h4 class="msg160-title">
										Ｂ４Ｌ（横）サイズ
									</h4><!-- ./msg200-title -->
									<h5 class="msg160-size-title"><span class="msg160-icon">●</span>巾000 × 高さ000 × マチ000mm</h5>
									<p>大きめの雑貨や食品、衣料品を入れるのに適したサイズです。イベントなどで使うパンフレットを入れるのにも最適です。</p>
								</div><!-- ./text -->
							</div><!-- end message-160 -->
						</div><!-- ./primary-row-->	
						
						<div class="primary-row clearfix">
							<div class="message-left message-160 clearfix">
								<div class="image">
									<img src="<?php bloginfo('template_url'); ?>/img/content/basic_size04.jpg" alt="basic" />
								</div><!-- ./image -->
								<div class="text">
									<h4 class="msg160-title">
										Ａ４Ｌ（縦）サイズ
									</h4><!-- ./msg200-title -->
									<h5 class="msg160-size-title"><span class="msg160-icon">●</span>巾000 × 高さ000 × マチ000mm</h5>
									<p>アクセサリーなどのちょっとした小物に最適なサイズです。小さいサイズのバッグを取り揃えることで、バッグを渡す方の細やかな心遣いが伝わります。</p>
									<p>A4サイズがピッタリ入ります。</p>
								</div><!-- ./text -->
							</div><!-- end message-160 -->
						</div><!-- ./primary-row-->	
						
						<div class="primary-row clearfix">
							<div class="message-left message-160 clearfix">
								<div class="image">
									<img src="<?php bloginfo('template_url'); ?>/img/content/basic_size05.jpg" alt="basic" />
								</div><!-- ./image -->
								<div class="text">
									<h4 class="msg160-title">
										ＷＤＭ（縦）サイズ
									</h4><!-- ./msg200-title -->
									<h5 class="msg160-size-title"><span class="msg160-icon">●</span>巾000 × 高さ000 × マチ000mm</h5>
									<p>ブライダル紙袋に適したサイズです。</p>
									<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
								</div><!-- ./text -->
							</div><!-- end message-160 -->
						</div><!-- ./primary-row-->	
						
						<div class="primary-row clearfix">
							<div class="message-left message-160 clearfix">
								<div class="image">
									<img src="<?php bloginfo('template_url'); ?>/img/content/basic_size06.jpg" alt="basic" />
								</div><!-- ./image -->
								<div class="text">
									<h4 class="msg160-title">
										ＷＤＭ（縦）サイズ
									</h4><!-- ./msg200-title -->
									<h5 class="msg160-size-title"><span class="msg160-icon">●</span>巾000 × 高さ000 × マチ000mm</h5>
									<p>ブライダル紙袋に適したサイズです。</p>
									<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
								</div><!-- ./text -->
							</div><!-- end message-160 -->
						</div><!-- ./primary-row-->	
						
						<div class="primary-row clearfix">
							<div class="message-left message-160 clearfix">
								<div class="image">
									<img src="<?php bloginfo('template_url'); ?>/img/content/basic_size07.jpg" alt="basic" />
								</div><!-- ./image -->
								<div class="text">
									<h4 class="msg160-title">
										Ａ５Ｍ（縦）サイズ
									</h4><!-- ./msg200-title -->
									<h5 class="msg160-size-title"><span class="msg160-icon">●</span>巾000 × 高さ000 × マチ000mm</h5>
									<p>小物を入れるのに適したサイズです。アクセサリーなどのちょっとした小物に最適なサイズです。小さいサイズのバッグを取り揃えることで、バッグを渡す方の細やかな心遣いが伝わります。A4サイズがピッタリ入ります。</p>
								</div><!-- ./text -->
							</div><!-- end message-160 -->
						</div><!-- ./primary-row-->	
						
						<div class="primary-row clearfix">
							<div class="message-left message-160 clearfix">
								<div class="image">
									<img src="<?php bloginfo('template_url'); ?>/img/content/basic_size08.jpg" alt="basic" />
								</div><!-- ./image -->
								<div class="text">
									<h4 class="msg160-title">
										CLサイズ
									</h4><!-- ./msg200-title -->
									<h5 class="msg160-size-title"><span class="msg160-icon">●</span>巾000 × 高さ000 × マチ000mm</h5>
									<p>ケーキを入れるのに適したサイズです。</p>
									<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
								</div><!-- ./text -->
							</div><!-- end message-160 -->
						</div><!-- ./primary-row-->	
						
						<div class="primary-row clearfix">
							<div class="message-left message-160 clearfix">
								<div class="image">
									<img src="<?php bloginfo('template_url'); ?>/img/content/basic_size09.jpg" alt="basic" />
								</div><!-- ./image -->
								<div class="text">
									<h4 class="msg160-title">
										ＷＮＬサイズ
									</h4><!-- ./msg200-title -->
									<h5 class="msg160-size-title"><span class="msg160-icon">●</span>巾000 × 高さ000 × マチ000mm</h5>
									<p>ワインを入れるのに適したサイズです。</p>
									<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
								</div><!-- ./text -->
							</div><!-- end message-160 -->
						</div><!-- ./primary-row-->	
						
						<div class="primary-row clearfix">
							<div class="message-left message-160 clearfix">
								<div class="image">
									<img src="<?php bloginfo('template_url'); ?>/img/content/basic_size10.jpg" alt="basic" />
								</div><!-- ./image -->
								<div class="text">
									<h4 class="msg160-title">
										その他サイズ
									</h4><!-- ./msg200-title -->
									<h5 class="msg160-size-title"><span class="msg160-icon">●</span>巾000 × 高さ000 × マチ000mm</h5>
									<p>上記規格サイズ以外でも対応いたします。なお、サイズや形態を変更される際には、z価格が変わる場合がございますので、事前にお問い合わせください。</p>
								</div><!-- ./text -->
							</div><!-- end message-160 -->
						</div><!-- ./primary-row-->	
						
						<h3 class="basic-title clearfix"><span class="basic-tstep">Step.3</span><span class="basic-ttext">印刷の方法を選ぶ</span></h3>
						<p>手提げ袋のサイズが決まったら、次は印刷の色数をお選びください。</p>
						<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ</p>
						<div class="primary-row clearfix">
							<div class="message-left message-160 clearfix">
								<div class="image">
									<img src="<?php bloginfo('template_url'); ?>/img/content/basic_print01.jpg" alt="basic" />
								</div><!-- ./image -->
								<div class="text">
									<h4 class="msg160-title">
										箔押し印刷
									</h4><!-- ./msg200-title -->
									<p>ホットスタンプ印刷ともいわれております。</p>
									<p>箔の種類だけの限定されますが、金・銀・ホログラムなど他の方法では表現できない効果があります。</p>									
								</div><!-- ./text -->
							</div><!-- end message-160 -->
						</div><!-- ./primary-row-->	
						
						<div class="primary-row clearfix">
							<div class="message-left message-160 clearfix">
								<div class="image">
									<img src="<?php bloginfo('template_url'); ?>/img/content/basic_print02.jpg" alt="basic" />
								</div><!-- ./image -->
								<div class="text">
									<h4 class="msg160-title">
										名入れ印刷
									</h4><!-- ./msg200-title -->
									<p>ロゴマークや名前のみ入れたいという方にオススメの印刷方法です。</p>									
								</div><!-- ./text -->
							</div><!-- end message-160 -->
						</div><!-- ./primary-row-->	
						
						<h3 class="basic-title clearfix"><span class="basic-tstep">Step.4</span><span class="basic-ttext">ハンドルを選ぶ</span></h3>
						<p>ハンドルは、オリジナル手提げ袋にとってアクセントとなる重要な部分。種類によって、袋の印象を大きく変えることができます。印刷色・デザインと合わせてお選びください。</p>
						<div class="primary-row clearfix">
							<div class="message-group message-col160"><!-- begin message-group -->
								<div class="message-row clearfix"><!--message-row -->
									<div class="message-col">										
										<div class="image">
											<img src="<?php bloginfo('template_url'); ?>/img/content/basic_handle01.jpg" alt="basic" />
										</div><!-- end image -->    
										<div class="text">
											<h4 class="cl160-title">ハッピーグリップ</h4>
											<p>袋の手付用に樹脂成型した物です。色は１０色あります。</p>
										</div>            
									</div><!-- end message-col -->									            
									
									<div class="message-col">										
										<div class="image">
											<img src="<?php bloginfo('template_url'); ?>/img/content/basic_handle02.jpg" alt="basic" />
										</div><!-- end image -->    
										<div class="text">
											<h4 class="cl160-title">スピンドル</h4>
											<p>糸を編んだ紐で、手触りが良く持ちやすい紐です。袋の寸法によって２種類の太さ・１２色から選びます。</p>
										</div>            
									</div><!-- end message-col -->	
									
									<div class="message-col">										
										<div class="image">
											<img src="<?php bloginfo('template_url'); ?>/img/content/basic_handle03.jpg" alt="basic" />
										</div><!-- end image -->    
										<div class="text">
											<h4 class="cl160-title">リボン</h4>
											<p>２０～３０ｍｍのリボンで高級な袋になります。シワが付きやすい場合があります。製品ごとにリボンを変える事ができます。（別見積）</p>
										</div>            
									</div><!-- end message-col -->	
									
									<div class="message-col">										
										<div class="image">
											<img src="<?php bloginfo('template_url'); ?>/img/content/basic_handle04.jpg" alt="basic" />
										</div><!-- end image -->    
										<div class="text">
											<h4 class="cl160-title">平紐</h4>
											<p>布の紐で２０ｍｍ幅が１０色あります。</p>
										</div>            
									</div><!-- end message-col -->	
								</div><!-- end message-row -->
							</div><!-- end message-group -->
						</div><!-- ./primary-row -->
												
						<h3 class="basic-title clearfix"><span class="basic-tstep">Step.5</span><span class="basic-ttext">オプションを選ぶ</span></h3>
						<p>その他にも、紙袋本来の機能を超えたスペシャルな紙袋を作りたい方にオプションをご用意しております。</p>
						<div class="primary-row clearfix">
							<div class="message-group message-col160"><!-- begin message-group -->
								<div class="message-row clearfix"><!--message-row -->
									<div class="message-col">										
										<div class="image">
											<img src="<?php bloginfo('template_url'); ?>/img/content/basic_option01.jpg" alt="basic" />
										</div><!-- end image -->    
										<div class="text">
											<h4 class="cl160-title">ＰＰ加工</h4>
											<p>ツヤ感のあるフィルムを、印刷した紙にラミネート加工します。</p>
											<p>光沢のある仕上がりです。</p>
										</div>            
									</div><!-- end message-col -->									            
									
									<div class="message-col">										
										<div class="image">
											<img src="<?php bloginfo('template_url'); ?>/img/content/basic_option02.jpg" alt="basic" />
										</div><!-- end image -->    
										<div class="text">
											<h4 class="cl160-title">エンボス加工</h4>
											<p>紙にさまざまな柄や模様を押し付けて、凹凸をつける加工です。他とは違う手触り感を楽しむことができ、高級感もアップします。</p>
										</div>            
									</div><!-- end message-col -->	
									
									<div class="message-col">										
										<div class="image">
											<img src="<?php bloginfo('template_url'); ?>/img/content/basic_option03.jpg" alt="basic" />
										</div><!-- end image -->    
										<div class="text">
											<h4 class="cl160-title">ハトメ</h4>
											<p>ハトメとは、紙袋に紐を通す部分につける金具です。 持ち手部分の強度が強くなるので、重いものを入れる紙袋に最適です。</p>
										</div>            
									</div><!-- end message-col -->	
									
									<div class="message-col">										
										<div class="image">
											<img src="<?php bloginfo('template_url'); ?>/img/content/basic_option04.jpg" alt="basic" />
										</div><!-- end image -->    
										<div class="text">
											<h4 class="cl160-title">浮き出し</h4>
											<p>紙袋の表面からぷくっと浮き出した表現が可能です。ロゴやデザインに立体感を持たせ、印象をさらに高めることができます。</p>
										</div>            
									</div><!-- end message-col -->	
								</div><!-- end message-row -->
							</div><!-- end message-group -->
						</div><!-- ./primary-row -->
						<div class="basic-bottom-text">
							<a href="#tab-form">
								<img src="<?php bloginfo('template_url'); ?>/img/content/basic-btn.jpg" alt="basic" />
							</a>
						</div>
					</div><!-- end primary-box -->					
				</div><!-- end tab1-->
				<div id="tab2">
					<div class="primary-row clearfix">
						<?php echo do_shortcode('[contact-form-7 id="284" title="Basic_Order"]') ?>
					</div><!-- end primary-row -->					
				</div><!-- end tab2 -->				
			</div><!-- end primary-box -->
		</div><!-- end part-product-box -->			
	<script src="http://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/ajaxzip3.js" charset="UTF-8"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#zip').change(function(){					
			//AjaxZip3.JSONDATA = "https://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/zipdata";
			AjaxZip3.zip2addr(this,'','address','your-addr');
			});
			$('.htabs a').htabs();			
		});
	</script>	