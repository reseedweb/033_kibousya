<?php get_template_part('part','snavi'); ?>
<div class="sidebar-row clearfix"><!-- begin sidebar-row -->
    <div class="side-con"><!-- begin side-con -->
        <img alt="sidebar" src="<?php bloginfo('template_url');?>/img/common/side_contact_bg.jpg" />
        <div class="side-con-btn">
            <a href="<?php bloginfo('url'); ?>/contact">
                <img alt="sidebar" src="<?php bloginfo('template_url');?>/img/common/side_contact_btn.jpg" />
            </a>
        </div><!-- ./side-con-btn -->		
    </div><!-- end side-con -->
</div><!-- end sidebar-row -->

<div class="sidebar-row clearfix"><!-- begin sidebar-row -->
	<div class="side-sample">
		<a href="<?php bloginfo('url'); ?>/sample">
			<img src="<?php bloginfo('template_url'); ?>/img/common/side_content_sample.png" alt="sidebar" />
		</a>
	</div>	
</div><!-- end sidebar-row -->

<div class="sidebar-row clearfix"><!-- begin sidebar-row -->
    <div class="sideMenu"><!-- begin sideMenu -->        
		<h2 class="title">商品ラインナップ</h2>
			<ul class="sideMenu-content">
				<li>
					<a href="<?php bloginfo('url'); ?>/basic"><img src="<?php bloginfo('template_url'); ?>/img/common/side_menu_img1.jpg" alt="sidebar" /></a>
				</li>
				<li>
					<a href="<?php bloginfo('url'); ?>/semi"><img src="<?php bloginfo('template_url'); ?>/img/common/side_menu_img2.jpg" alt="sidebar" /></a>
				</li>
				<li>
					<a href="<?php bloginfo('url'); ?>/full"><img src="<?php bloginfo('template_url'); ?>/img/common/side_menu_img3.jpg" alt="sidebar" /></a>
				</li>
			</ul>        
    </div><!-- end sideMenu -->
</div><!-- end sidebar-row -->


<div class="sidebar-row clearfix"><!-- begin sidebar-row -->
    <div class="side-basic"><!-- begin side-basic -->
        <img alt="sidebar" src="<?php bloginfo('template_url');?>/img/common/side_basic_bg.jpg" />
        <div class="side-basic-btn">
            <a href="<?php bloginfo('url'); ?>/basic">
                <img alt="sidebar" src="<?php bloginfo('template_url');?>/img/common/side_basic_btn.jpg"/>
            </a>
        </div><!-- ./side-basic-btn -->		
    </div><!-- end side-basic -->
</div><!-- end sidebar-row -->


<div class="sidebar-row clearfix"><!-- begin sidebar-row -->
    <div class="side-bridal"><!-- begin side-bridal -->
        <img alt="sidebar" src="<?php bloginfo('template_url');?>/img/common/side_bridal_bg.jpg" />
        <div class="side-bridal-btn">
            <a href="<?php bloginfo('url'); ?>/bridal">
                <img alt="sidebar" src="<?php bloginfo('template_url');?>/img/common/side_bridal_btn.jpg"/>
            </a>
        </div><!-- ./side-bridal-btn -->		
    </div><!-- end side-bridal -->
</div><!-- end sidebar-row -->

<div class="sidebar-row clearfix"><!-- begin sidebar-row -->
    <div class="side-about"><!-- begin side-about -->
        <img alt="sidebar" src="<?php bloginfo('template_url');?>/img/common/side_about_bg.jpg" />
        <div class="side-about-btn">
            <a href="<?php bloginfo('url'); ?>/about">
                <img alt="sidebar" src="<?php bloginfo('template_url');?>/img/common/side_about_btn.jpg"/>
            </a>
        </div><!-- ./side-about-btn -->		
    </div><!-- end side-about -->
</div><!-- end sidebar-row -->


<div class="sidebar-row clearfix"><!-- begin sidebar-row -->
    <div class="side-factory"><!-- begin side-factory -->
        <img alt="sidebar" src="<?php bloginfo('template_url');?>/img/common/side_factory_bg.jpg" />
        <div class="side-factory-btn">
            <a href="<?php bloginfo('url'); ?>/factory">
                <img alt="sidebar" src="<?php bloginfo('template_url');?>/img/common/side_factory_btn.jpg"/>
            </a>
        </div><!-- ./side-factory-btn -->		
    </div><!-- end side-factory -->
</div><!-- end sidebar-row -->
