<?php get_header(); ?>	
	<?php get_template_part('part','breadcrumb'); ?>	
	<div class="main-content clearfix"><!-- begin main-content -->
		<h1 class="basic-top-title">	
			ご注文の流れ
		</h1><!-- ./about-top-title -->
	</div>
	<div class="main-content clearfix"><!-- begin main-content -->
		 <div class="top-title clearfix">
			<h2 class="h2-title">ご注文の流れ</h2>    
			<h2 class="top-title-en">Flow</h2>		
		</div>	
		<p class="pt20">▽クリックで、各STEPにジャンプします</p>
		<div class="flow-content clearfix">
			<div class="part-flow-text1">
				お問い合わせから納品までに流れ
			</div>		
			<div class="part-flow">
				<a href="<?php bloginfo('url'); ?>/flow">
					<img src="<?php bloginfo('template_url'); ?>/img/content/top_part_flow.jpg" alt="top" />
				</a>
			</div>
			<ul class="part-flow-flist clearfix">
				<li>お問い合わせ</li>
				<li>お見積もりのご提示</li>
				<li>ご発注データ入稿</li>
				<li>海外の製造工場にて制作・検品</li>
				<li>完成した製品の輸入・通関業務</li>
				<li>商品の発送</li>
			</ul>			
		</div><!-- end flow-content -->						
	</div><!-- end main-content  -->
	
	<div class="primary-row clearfix">
		<div class="primary-row clearfix">			
			<h3 class="about-title clearfix">STEP1. お問い合わせ</h3>
			<div class="flow-info clearfix">
				<p>まずは当サイトをご覧頂き、ご注文される紙袋の仕様をご検討ください。ご注文の見積もりは専用のフォームから受付ております。どれを選べばよいかわからないという方は、下記のお問い合わせフォームから、お気軽にお問い合わせください。</p>
				<p>ご相談・お問い合わせは<span class="flow-note">こちら</span></p>
				<p>見積もりのご依頼はこちら<span class="flow-note">こちら</span></p>
			</div><!-- end flow-info -->
		</div><!-- ./primary-row -->	
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix">
		<div class="primary-row clearfix">			
			<h3 class="about-title clearfix">STEP2. 見積もりの提示</h3>
			<div class="flow-info clearfix">
				<p>依頼を確認後、メールまたはＦＡＸにて見積もりの提示をさせて頂きます。</p>
			</div><!-- end flow-info -->
		</div><!-- ./primary-row -->	
	</div><!-- end primary-row -->
		
	<div class="primary-row clearfix">
		<div class="primary-row clearfix">			
			<h3 class="about-title clearfix">STEP3. ご発注・データ入稿</h3>
			<div class="flow-info clearfix">
				<p>見積もりを確認いただき、合意をいただければ、正式に注文書を発行させて頂きます。部数、サイズの変更が生じる場合は、再度お見積もりさせていただきます。 デザインの原稿を、イラストレーター(ai)、フォトショップ(psd)のデータ、もしくはＦＡＸ(エクセル形式)で送って頂きます。デザインが決まってない場合は、弊社でデザインの制作をさせて頂くことも可能です。(別途デザイン費が必要です)</p>
				<p>データ入稿について、詳しくは<span class="flow-note">こちら</span></p>
			</div><!-- end flow-info -->
		</div><!-- ./primary-row -->	
	</div><!-- end primary-row -->
		
	<div class="primary-row clearfix">
		<div class="primary-row clearfix">			
			<h3 class="about-title clearfix">STEP4. 海外工場で製造</h3>
			<div class="flow-info clearfix">
				<p>デザインの決定後、海外の工場にて製造を開始します。 完成まで最短４日から２ヶ月ほどお時間を頂いております。納期はお客様のご相談によって調整させていただくことも可能です。 サンプル品、校正を希望のお客様は別途費用でサンプルをお作りします。</p>
			</div><!-- end flow-info -->
		</div><!-- ./primary-row -->	
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix">
		<div class="primary-row clearfix">			
			<h3 class="about-title clearfix">STEP5. 完成製品の輸入・通関</h3>
			<div class="flow-info clearfix">
				<p>製造が完了いたしましたら、海外より製品を輸入します。</p>
			</div><!-- end flow-info -->
		</div><!-- ./primary-row -->	
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix">
		<div class="primary-row clearfix">			
			<h3 class="about-title clearfix">STEP6. 商品の発送</h3>
			<div class="flow-info clearfix">
				<p>お客様のもとへ商品を発送いたします。商品をお確かめくださいませ。</p>
			</div><!-- end flow-info -->
		</div><!-- ./primary-row -->	
	</div><!-- end primary-row -->
	
	<div class="flow-contact-bg">
		<div class="flow-contact-btn">
			<a href="<?php bloginfo('url'); ?>/contact">
				<img src="<?php bloginfo('template_url'); ?>/img/content/top_part_con_btn.jpg" alt="top" />
			</a>
		</div>
	</div>
<?php get_footer(); ?>
