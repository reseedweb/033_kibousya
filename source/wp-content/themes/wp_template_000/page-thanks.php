<?php get_header(); ?>	
	<?php get_template_part('part','breadcrumb'); ?>	
    <div class="primary-row clearfix"><!-- begin primary-row -->
		<h1 class="basic-top-title">	
			送信完了しました
		</h1>
		<p class="ln2em">
			お問い合わせいただきありがとうございました。スタッフがメール内容を確認し、2営業日以内にご返信致します。<br />
			1週間経っても返信がない・お急ぎの場合は、お手数ですがお電話にてご連絡ください。
		</p>
    </div><!-- end primary-row -->
	<div class="primary-row clearfix"><!-- begin primary-row -->
	<?php get_template_part('part','contact'); ?>
	</div><!-- end primary-row -->	
<?php get_footer(); ?>