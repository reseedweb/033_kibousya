<?php get_header(); ?>	
	<?php get_template_part('part','breadcrumb'); ?>	
	<div class="main-content clearfix"><!-- begin main-content -->
		<h1 class="basic-top-title">	
			スタッフブログ
		</h1><!-- ./basic-top-title -->
		<p>オリジナル紙袋制作サイト「Only One」のスタッフブログです。最新の紙袋の制作事例などご紹介いたします。</p>
	</div>
    <?php query_posts(array('posts_per_page' => 5, 'paged' => get_query_var('paged') )); ?>
    <?php if (have_posts()) : ?>
        <?php while (have_posts()) : the_post(); ?>    
        <!-- do stuff ... -->
        <div class="primary-row clearfix">            
            <h2 class="factory-title"><?php the_title(); ?></h2>         
            <div class="post-row-content">                
                <div class="post-row-meta">
                    <i class="fa fa-clock-o"><?php the_time('l, F jS, Y'); ?></i>
                    <i class="fa fa-tags"></i><?php the_category(' , ', get_the_id()); ?>
                    <i class="fa fa-user"></i><?php the_author_link(); ?>
                </div>
                <div class="post-row-description"><?php the_content(); ?></div>                    
            </div>
        </div>
        <?php endwhile; ?>    
        <div class="primary-row">
            <?php if(function_exists('wp_pagenavi')) wp_pagenavi(); ?>
        </div>
    <?php endif; ?>
    <?php wp_reset_query(); ?> 
	<?php get_template_part('part','contact'); ?>	
<?php get_footer(); ?>