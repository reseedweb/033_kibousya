			<nav>
                <div id="gNavi" class="wrapper">
                    <ul class="dynamic clearfix">
                        <li>
                        <a href="<?php bloginfo('url'); ?>/">
                            <span class="first">ホーム</span>
                            <span class="second">HOME</span>
                        </a>
                    </li>
                    <li>
                        <a href="#product">
                            <span class="first">商品ラインナップ</span>
                            <span class="second">PRODUCT</span>
                        </a>
                    </li>                            
                    <li>
                        <a href="<?php bloginfo('url'); ?>/flow">
                            <span class="first">ご注文の流れ</span>
                            <span class="second">FLOW</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php bloginfo('url'); ?>/price">
                            <span class="first">参考価格例</span>
                            <span class="second">PRICE</span>
                        </a>
                    </li>    
                    <li>
                        <a href="<?php bloginfo('url'); ?>/faq">
                            <span class="first">よくあるご質問</span>
                            <span class="second">FAQ</span>
                        </a>
                    </li> 
					 <li>
                        <a href="<?php bloginfo('url'); ?>/company">
                            <span class="first">会社案内</span>
                            <span class="second">COMPANY</span>
                        </a>
                    </li> 					
                    </ul>
                </div>
            </nav>