<?php get_header(); ?>	
	<?php get_template_part('part','breadcrumb'); ?>	
	<div class="main-content clearfix"><!-- begin main-content -->
		<h1 class="basic-top-title">	
			データ入稿について
		</h1><!-- ./basic-top-title -->
		<p>紙袋制作サイト「Only One」を運営している会社についてご紹介いたします。</p>		
		<div class="mt20 clearfix">
			<h2 class="factory-title">入稿方法</h2>		
			<p>デザイン原稿の入稿方法は下記の２種類からお選び頂けます。</p>
			<p>&nbsp;・Adobe Illustrator CCまで (ai形式/eps形式)</p>
			<p>&nbsp;・Adobe Photoshop CCまで (psd形式/eps形式)</p>
			<p>(バージョンはCS5まで対応しております。)</p>
		</div>
		
		<div class="primary-row clearfix">
			<h2 class="factory-title">データ作成についてのご注意 (必ずお読み下さい)</h2>
			<p>ご注意</p>
			<p>&nbsp;・データの作成は、原寸にてお願いします。</p>
			<p>&nbsp;・「4色フルカラー」をご希望の方は、カラーモードはCMYKを選択してください。</p>
			<p>&nbsp;・特色指定の場合はDIC、もしくはPANTONEにてご指定下さい。</p>
			<p>&nbsp;・カラーモードはCMYKを選択してください。</p>
			<p>&nbsp;・1色刷り面はグレースケールで作成してください。</p>
			<p>&nbsp;・解像度は350dpi以上にしてください。(解像度が引く場合は画像が荒くなる可能性があります。)</p>
			<p>&nbsp;・Illustratorに配置した画像はすべて「埋め込み」にしてください。</p>
			<p>&nbsp;・フォントは必ず「文字のアウトライン化」をしてください。</p>
			<p>&nbsp;・添付ファイルはできるだけ圧縮して、容量は最大3MBくらいにして頂けるようにお願いします。</p>
		</div><!-- primary-row -->
	</div><!-- end main-content -->
		
	<div class="primary-row clearfix"><!-- begin primary-row -->	
		<h2 class="factory-title">テンプレートダウンロード</h2>
		<p>入稿用のテンプレートはIllustrator、Photoshopの２タイプをご用意しております。</p>
		<p>お手持ちのソフトで操作可能なデータをダウンロードし、イメージを作成して頂いた上でご入稿ください。</p>
		<p>ダウンロードにより生じたいかなる障害も、当社では保障いたしかねます。お客様の責任のもとでご利用ください。</p>
		<table class="draft-table">
			<tr>
				<th>
					BLHサイズ<br />
					<span class="draft-size">(高さ45、幅35、奥行22)</span>
				</th>
				<td><img src="<?php bloginfo('template_url'); ?>/img/content/draft_content_img1.jpg" alt="draft" /></td>
				<td>
					<p>
						<a href="<?php bloginfo('url'); ?>/contact">
							<img src="<?php bloginfo('template_url'); ?>/img/content/draft_content_ps.jpg" alt="draft" /><span class="draft-software">Adobe Photoshopデータ ダウンロード</span>
						</a>
					</p>
					<p class="draft-icon">
						<a href="<?php bloginfo('url'); ?>/contact">
							<img src="<?php bloginfo('template_url'); ?>/img/content/draft_content_ai.jpg" alt="draft" /><span class="draft-software">Adobe Illustratorデータ ダウンロード</span>
						</a>
					</p>
				</td>
			</tr>
			
			<tr>
				<th>
					BLSサイズ<br />
					<span class="draft-size">(高さ35、幅35、奥行22)</span>
				</th>
				<td><img src="<?php bloginfo('template_url'); ?>/img/content/draft_content_img2.jpg" alt="draft" /></td>
				<td>
					<p>
						<a href="<?php bloginfo('url'); ?>/contact">
							<img src="<?php bloginfo('template_url'); ?>/img/content/draft_content_ps.jpg" alt="draft" /><span class="draft-software">Adobe Photoshopデータ ダウンロード</span>
						</a>
					</p>
					<p class="draft-icon">
						<a href="<?php bloginfo('url'); ?>/contact">
							<img src="<?php bloginfo('template_url'); ?>/img/content/draft_content_ai.jpg" alt="draft" /><span class="draft-software">Adobe Illustratorデータ ダウンロード</span>
						</a>
					</p>
				</td>
			</tr>
			
			<tr>
				<th>
					BLYサイズ<br />
					<span class="draft-size">(高さ30、幅42、奥行22)</span>
				</th>
				<td><img src="<?php bloginfo('template_url'); ?>/img/content/draft_content_img3.jpg" alt="draft" /></td>
				<td>
					<p>
						<a href="<?php bloginfo('url'); ?>/contact">
							<img src="<?php bloginfo('template_url'); ?>/img/content/draft_content_ps.jpg" alt="draft" /><span class="draft-software">Adobe Photoshopデータ ダウンロード</span>
						</a>
					</p>
					<p class="draft-icon">
						<a href="<?php bloginfo('url'); ?>/contact">
							<img src="<?php bloginfo('template_url'); ?>/img/content/draft_content_ai.jpg" alt="draft" /><span class="draft-software">Adobe Illustratorデータ ダウンロード</span>
						</a>
					</p>
				</td>
			</tr>
			
			<tr>
				<th>
					BMSサイズ<br />
					<span class="draft-size">(高さ35、幅32、奥行18)</span>
				</th>
				<td><img src="<?php bloginfo('template_url'); ?>/img/content/draft_content_img4.jpg" alt="draft" /></td>
				<td>
					<p>
						<a href="<?php bloginfo('url'); ?>/contact">
							<img src="<?php bloginfo('template_url'); ?>/img/content/draft_content_ps.jpg" alt="draft" /><span class="draft-software">Adobe Photoshopデータ ダウンロード</span>
						</a>
					</p>
					<p class="draft-icon">
						<a href="<?php bloginfo('url'); ?>/contact">
							<img src="<?php bloginfo('template_url'); ?>/img/content/draft_content_ai.jpg" alt="draft" /><span class="draft-software">Adobe Illustratorデータ ダウンロード</span>
						</a>
					</p>
				</td>
			</tr>
			
			<tr>
				<th>
					MLサイズ<br />
					<span class="draft-size">(高さ44、幅32、奥行11)</span>
				</th>
				<td><img src="<?php bloginfo('template_url'); ?>/img/content/draft_content_img5.jpg" alt="draft" /></td>
				<td>
					<p>
						<a href="<?php bloginfo('url'); ?>/contact">
							<img src="<?php bloginfo('template_url'); ?>/img/content/draft_content_ps.jpg" alt="draft" /><span class="draft-software">Adobe Photoshopデータ ダウンロード</span>
						</a>
					</p>
					<p class="draft-icon">
						<a href="<?php bloginfo('url'); ?>/contact">
							<img src="<?php bloginfo('template_url'); ?>/img/content/draft_content_ai.jpg" alt="draft" /><span class="draft-software">Adobe Illustratorデータ ダウンロード</span>
						</a>
					</p>
				</td>
			</tr>
			
			<tr>
				<th>
					MLSサイズ<br />
					<span class="draft-size">(高さ32、幅32、奥行11)</span>
				</th>
				<td><img src="<?php bloginfo('template_url'); ?>/img/content/draft_content_img6.jpg" alt="draft" /></td>
				<td>
					<p>
						<a href="<?php bloginfo('url'); ?>/contact">
							<img src="<?php bloginfo('template_url'); ?>/img/content/draft_content_ps.jpg" alt="draft" /><span class="draft-software">Adobe Photoshopデータ ダウンロード</span>
						</a>
					</p>
					<p class="draft-icon">
						<a href="<?php bloginfo('url'); ?>/contact">
							<img src="<?php bloginfo('template_url'); ?>/img/content/draft_content_ai.jpg" alt="draft" /><span class="draft-software">Adobe Illustratorデータ ダウンロード</span>
						</a>
					</p>
				</td>
			</tr>
			
			<tr>
				<th>
					MLYサイズ<br />
					<span class="draft-size">(高さ29、幅35、奥行11)</span>
				</th>
				<td><img src="<?php bloginfo('template_url'); ?>/img/content/draft_content_img7.jpg" alt="draft" /></td>
				<td>
					<p>
						<a href="<?php bloginfo('url'); ?>/contact">
							<img src="<?php bloginfo('template_url'); ?>/img/content/draft_content_ps.jpg" alt="draft" /><span class="draft-software">Adobe Photoshopデータ ダウンロード</span>
						</a>
					</p>
					<p class="draft-icon">
						<a href="<?php bloginfo('url'); ?>/contact">
							<img src="<?php bloginfo('template_url'); ?>/img/content/draft_content_ai.jpg" alt="draft" /><span class="draft-software">Adobe Illustratorデータ ダウンロード</span>
						</a>
					</p>
				</td>
			</tr>
			
			<tr>
				<th>
					MMYサイズ<br />
					<span class="draft-size">(高さ27、幅32、奥行11)</span>
				</th>
				<td><img src="<?php bloginfo('template_url'); ?>/img/content/draft_content_img8.jpg" alt="draft" /></td>
				<td>
					<p>
						<a href="<?php bloginfo('url'); ?>/contact">
							<img src="<?php bloginfo('template_url'); ?>/img/content/draft_content_ps.jpg" alt="draft" /><span class="draft-software">Adobe Photoshopデータ ダウンロード</span>
						</a>
					</p>
					<p class="draft-icon">
						<a href="<?php bloginfo('url'); ?>/contact">
							<img src="<?php bloginfo('template_url'); ?>/img/content/draft_content_ai.jpg" alt="draft" /><span class="draft-software">Adobe Illustratorデータ ダウンロード</span>
						</a>
					</p>
				</td>
			</tr>
			
			<tr>
				<th>
					MMHサイズ<br />
					<span class="draft-size">(高さ35、幅27、奥行11)</span>
				</th>
				<td><img src="<?php bloginfo('template_url'); ?>/img/content/draft_content_img9.jpg" alt="draft" /></td>
				<td>
					<p>
						<a href="<?php bloginfo('url'); ?>/contact">
							<img src="<?php bloginfo('template_url'); ?>/img/content/draft_content_ps.jpg" alt="draft" /><span class="draft-software">Adobe Photoshopデータ ダウンロード</span>
						</a>
					</p>
					<p class="draft-icon">
						<a href="<?php bloginfo('url'); ?>/contact">
							<img src="<?php bloginfo('template_url'); ?>/img/content/draft_content_ai.jpg" alt="draft" /><span class="draft-software">Adobe Illustratorデータ ダウンロード</span>
						</a>
					</p>
				</td>
			</tr>
			
			<tr>
				<th>
					SMHサイズ<br />
					<span class="draft-size">(高さ28、幅21、奥行7)</span>
				</th>
				<td><img src="<?php bloginfo('template_url'); ?>/img/content/draft_content_img10.jpg" alt="draft" /></td>
				<td>
					<p>
						<a href="<?php bloginfo('url'); ?>/contact">
							<img src="<?php bloginfo('template_url'); ?>/img/content/draft_content_ps.jpg" alt="draft" /><span class="draft-software">Adobe Photoshopデータ ダウンロード</span>
						</a>
					</p>
					<p class="draft-icon">
						<a href="<?php bloginfo('url'); ?>/contact">
							<img src="<?php bloginfo('template_url'); ?>/img/content/draft_content_ai.jpg" alt="draft" /><span class="draft-software">Adobe Illustratorデータ ダウンロード</span>
						</a>
					</p>
				</td>
			</tr>
			
			<tr>
				<th>
					WNサイズ<br />
					<span class="draft-size">(高さ30、幅9、奥行9)</span>
				</th>
				<td><img src="<?php bloginfo('template_url'); ?>/img/content/draft_content_img11.jpg" alt="draft" /></td>
				<td>
					<p>
						<a href="<?php bloginfo('url'); ?>/contact">
							<img src="<?php bloginfo('template_url'); ?>/img/content/draft_content_ps.jpg" alt="draft" /><span class="draft-software">Adobe Photoshopデータ ダウンロード</span>
						</a>
					</p>
					<p class="draft-icon">
						<a href="<?php bloginfo('url'); ?>/contact">
							<img src="<?php bloginfo('template_url'); ?>/img/content/draft_content_ai.jpg" alt="draft" /><span class="draft-software">Adobe Illustratorデータ ダウンロード</span>
						</a>
					</p>
				</td>
			</tr>
		</table>
	</div><!-- end primary-row -->	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->	
		<h2 class="factory-title">データ入稿はこちら</h2>
		<div class="draft-btn">
			<img src="<?php bloginfo('template_url'); ?>/img/content/draft_content_btn.jpg" alt="draft" />			
		</div>
		<div class="draft-contact">
			<p>デザインデータはこちらから入稿してください。</p>
			<p>メールアドレス： <span class="draft-email">hk-ochi@kibo-sha.com</span></p>
		</div>		
	</div><!-- end primary-row -->	
	<?php get_template_part('part','contact'); ?>	
<?php get_footer(); ?>