<?php get_template_part('part','snavi'); ?>

<div class="sidebar-row clearfix"><!-- begin sidebar-row -->
    <div class="sideMenu"><!-- begin sideMenu -->        
		<h2 class="title">商品ラインナップ</h2>
			<ul class="sideMenu-content">
				<li>
					<a href="<?php bloginfo('url'); ?>/basic"><img src="<?php bloginfo('template_url'); ?>/img/common/side_menu_img1.jpg" alt="sidebar" /></a>
				</li>
				<li>
					<a href="<?php bloginfo('url'); ?>/semi"><img src="<?php bloginfo('template_url'); ?>/img/common/side_menu_img2.jpg" alt="sidebar" /></a>
				</li>
				<li>
					<a href="<?php bloginfo('url'); ?>/full"><img src="<?php bloginfo('template_url'); ?>/img/common/side_menu_img3.jpg" alt="sidebar" /></a>
				</li>
			</ul>        
    </div><!-- end sideMenu -->
</div><!-- end sidebar-row -->

<div class="sidebar-row clearfix">
	<div class="sideBlog">
		<h4 class="sideBlog-title">最新の投稿</h4>
		<ul>
			<?php query_posts("posts_per_page=5"); ?><?php if(have_posts()):while(have_posts()):the_post(); ?>
			<li>
			<p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
			</li>
			<?php endwhile;endif; ?>
		</ul> 
	</div>
	<div class="sideBlog">
	    <h4 class="sideBlog-title">最新の投稿</h4>
	      <ul class="category">
			<?php wp_list_categories('sort_column=name&optioncount=0&hierarchical=1&title_li='); ?>
	      </ul>                             	
	</div>
	<div class="sideBlog">
      <h4 class="sideBlog-title">最新の投稿</h4>
      <ul><?php wp_get_archives('type=monthly&limit=12'); ?></ul>                               	
	</div>
</div>
