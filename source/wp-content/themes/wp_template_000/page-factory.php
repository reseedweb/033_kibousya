<?php get_header(); ?>	
	<?php get_template_part('part','breadcrumb'); ?>	
	<div class="main-content clearfix"><!-- begin main-content -->
		<h1 class="basic-top-title">	
			工場紹介
		</h1><!-- ./basic-top-title -->
		<p>オリジナル紙袋を制作している自社工場の様子をご紹介します。</p>		
		<div class="mt20 clearfix">
			<h2 class="factory-title">オリジナル紙袋自社工場のご紹介</h2>		
			<div class="message-right message-375 clearfix">
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/factory_mainImage01.jpg" alt="factory" />
				</div><!-- ./image -->
				<div class="text">
					テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト
				</div><!-- ./text -->
			</div><!-- end message-375 -->
		</div>
		
		<div class="primary-row clearfix">
			<h2 class="factory-title">上海工場内の様子</h2>
			<div class="factory-process-text">お客様にご依頼頂いたオリジナル紙袋を、弊社スタッフが一つひとつ丁寧に製造いたします。</div>
			<div class="message-group message-col240"><!-- begin message-group -->
				<div class="message-row clearfix"><!--message-row -->
					<div class="message-col">						
						<div class="image">
							<img src="<?php bloginfo('template_url'); ?>/img/content/factory_image01.jpg" alt="factory" />
						</div><!-- end image -->    
						<div class="text">
							工場内作業の様子1
						</div>            
					</div><!-- end message-col -->
					<div class="message-col">						
						<div class="image">
							<img src="<?php bloginfo('template_url'); ?>/img/content/factory_image02.jpg" alt="factory" />
						</div><!-- end image -->    
						<div class="text">
							工場内作業の様子2
						</div>            
					</div><!-- end message-col -->
					<div class="message-col">						
						<div class="image">
							<img src="<?php bloginfo('template_url'); ?>/img/content/factory_image03.jpg" alt="factory" />
						</div><!-- end image -->    
						<div class="text">
							工場内作業の様子3
						</div>            
					</div><!-- end message-col -->            
				</div><!-- end message-row -->
				
				<div class="message-row clearfix"><!--message-row -->
					<div class="message-col">						
						<div class="image">
							<img src="<?php bloginfo('template_url'); ?>/img/content/factory_image04.jpg" alt="factory" />
						</div><!-- end image -->    
						<div class="text">
							工場内作業の様子4
						</div>            
					</div><!-- end message-col -->
					<div class="message-col">						
						<div class="image">
							<img src="<?php bloginfo('template_url'); ?>/img/content/factory_image05.jpg" alt="factory" />
						</div><!-- end image -->    
						<div class="text">
							工場内作業の様子5
						</div>            
					</div><!-- end message-col -->
					<div class="message-col">						
						<div class="image">
							<img src="<?php bloginfo('template_url'); ?>/img/content/factory_image06.jpg" alt="factory" />
						</div><!-- end image -->    
						<div class="text">
							工場内作業の様子6
						</div>            
					</div><!-- end message-col -->            
				</div><!-- end message-row -->
								
				<div class="message-row clearfix"><!--message-row -->
					<div class="message-col">						
						<div class="image">
							<img src="<?php bloginfo('template_url'); ?>/img/content/factory_image07.jpg" alt="factory" />
						</div><!-- end image -->    
						<div class="text">
							工場内作業の様子7
						</div>            
					</div><!-- end message-col -->
					<div class="message-col">						
						<div class="image">
							<img src="<?php bloginfo('template_url'); ?>/img/content/factory_image08.jpg" alt="factory" />
						</div><!-- end image -->    
						<div class="text">
							工場内作業の様子8
						</div>            
					</div><!-- end message-col -->
					<div class="message-col">						
						<div class="image">
							<img src="<?php bloginfo('template_url'); ?>/img/content/factory_image09.jpg" alt="factory" />
						</div><!-- end image -->    
						<div class="text">
							工場内作業の様子9
						</div>            
					</div><!-- end message-col -->            
				</div><!-- end message-row -->
				
			</div><!-- end message-group -->
		</div><!-- primary-row -->
	</div><!-- end main-content -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->	
		<h2 class="factory-title">製造拠点のご紹介</h2>
		<div class="message-group message-col370"><!-- begin message-group -->
			<div class="message-row clearfix"><!--message-row -->
				<div class="message-col">					
					<div class="image">
						<iframe src="https://www.google.com/maps/embed?pb=!1m12!1m8!1m3!1d3415.116147152072!2d121.29364799999999!3d31.134289!3m2!1i1024!2i768!4f13.1!2m1!1z5LiK5rW35biM5pyb57SZ5bel5pyJ6ZmQ5YWs5Y-4!5e0!3m2!1sja!2sjp!4v1424767538604" width="370" height="255" frameborder="0" style="border:0"></iframe>
					</div><!-- end image -->    
					<div class="text">
						<h3 class="factory-addr-title">【上海希望紙工有限公司】</h3>
						<p>上海市松江高科技園区九亭</p>
					</div>            
				</div><!-- end message-col -->
				<div class="message-col">					
					<div class="image">
						<iframe src="https://www.google.com/maps/embed?pb=!1m12!1m8!1m3!1d3278.179725380873!2d135.539961!3d34.7510652!3m2!1i1024!2i768!4f13.1!2m1!1z5aSn6Ziq5bqc5aSn6Ziq5biC5p2x5reA5bed5Yy655Ge5YWJMi0xMy0zMQ!5e0!3m2!1sja!2sjp!4v1424767616982" width="370" height="255" frameborder="0" style="border:0"></iframe>
					</div><!-- end image -->    
					<div class="text">
						<h3 class="factory-addr-title">【大阪第2工場 】</h3>
						<p>〒533-0005 大阪市東淀川区瑞光2-12-11</p>
					</div>            
				</div><!-- end message-col -->				           
			</div><!-- end message-row -->
			
			<div class="message-row clearfix"><!--message-row -->
				<div class="message-col">					
					<div class="image">
						<iframe src="https://www.google.com/maps/embed?pb=!1m12!1m8!1m3!1d3280.769375863994!2d135.58669875!3d34.68576965!3m2!1i1024!2i768!4f13.1!2m1!1z44CSNTc3LTAwMDYg5p2x5aSn6Ziq5biC5qWg5qC5Mi0zLTE1!5e0!3m2!1sja!2sjp!4v1424767682915" width="370" height="255" frameborder="0" style="border:0"></iframe>
					</div><!-- end image -->    
					<div class="text">
						<h3 class="factory-addr-title">【東大阪物流センター 】</h3>
						<p>〒577-0006 東大阪市楠根2-3-15</p>
					</div>            
				</div><!-- end message-col -->
			</div><!-- end message-row -->
		</div><!-- end message-group -->
	</div><!-- end primary-row -->	
	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->	
		<h2 class="factory-title">主な保有設備</h2>
		<table class="factory-table">
			<tr>
				<th>製本加工一式</th>
				<td>全自動表紙貼り機、自動貼り箱機など</td>
			</tr>
			<tr>
				<th>製袋加工一式</th>
				<td>トムソン、箔押し器、穴あけ機など</td>
			</tr>
			<tr>
				<th>印刷加工一式</th>
				<td>全自動箔押し機、印刷機、製版機など</td>
			</tr>			
		</table>
	</div><!-- end primary-row -->	
	<?php get_template_part('part','contact'); ?>	
<?php get_footer(); ?>