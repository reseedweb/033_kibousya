			<section id="maintop"><!-- begin maintop -->   
				<div class="maintop-group maintop-col340 wrapper"><!-- begin maintop-group -->
					<div class="maintop-row clearfix"><!--maintop-row -->
						<div class="maintop-col">
							<div class="maintop-title">
								<div class="maintop-title1 basic-color">価格優先の方</div>
								<div class="maintop-title2">Basic Paper Bag</div>	
							</div>							
							<div class="maintop-img">
								<img src="<?php bloginfo('template_url'); ?>/img/top/top_box_img1.jpg" alt="maintop" />
							</div><!-- end image -->
							<div class="maintop-content">
								<div class="maintop-text clearfix">
									<div class="maintop-tinfo1"><span class="ttext-text1">納期</span><span class="ttext-text2 basic-color">最短２週間</span></div>
									<div class="maintop-tinfo2"><span class="ttext-text1">枚数</span><span class="ttext-text2 basic-color">100枚〜</span></div>
								</div>							
								<div class="maintop-btn">
									<a href="<?php bloginfo('url'); ?>/basic">
										<img src="<?php bloginfo('template_url'); ?>/img/top/top_box_btn1.jpg" alt="top"/>
									</a>
								</div>  
							</div>							          
						</div><!-- end maintop-col -->	

						<div class="maintop-col">
							<div class="maintop-title">
								<div class="maintop-title1 semi-color">納期優先の方</div>
								<div class="maintop-title2">Semi-order Paper Bag</div>	
							</div>							
							<div class="maintop-img">
								<img src="<?php bloginfo('template_url'); ?>/img/top/top_box_img2.jpg" alt="maintop" />
							</div><!-- end image -->
							<div class="maintop-content">
								<div class="maintop-text clearfix">
									<div class="maintop-tinfo1"><span class="ttext-text1">納期</span><span class="ttext-text2 semi-color">最短１週間</span></div>
									<div class="maintop-tinfo2"><span class="ttext-text1">枚数</span><span class="ttext-text2 semi-color">500枚〜</span></div>
								</div>							
								<div class="maintop-btn">
									<a href="<?php bloginfo('url'); ?>/semi">
										<img src="<?php bloginfo('template_url'); ?>/img/top/top_box_btn2.jpg" alt="top"/>
									</a>
								</div>  
							</div>
						</div><!-- end maintop-col -->	
						
						<div class="maintop-col">
							<div class="maintop-title">
								<div class="maintop-title1 full-color">品質優先の方</div>
								<div class="maintop-title2">Full order Paper Bag</div>	
							</div>							
							<div class="maintop-img">
								<img src="<?php bloginfo('template_url'); ?>/img/top/top_box_img3.jpg" alt="maintop" />
							</div><!-- end image -->
							<div class="maintop-content">
								<div class="maintop-text clearfix">
									<div class="maintop-tinfo1"><span class="ttext-text1">納期</span><span class="ttext-text2 full-color">最短45日</span></div>
									<div class="maintop-tinfo2"><span class="ttext-text1">枚数</span><span class="ttext-text2 full-color">500枚〜</span></div>
								</div>							
								<div class="maintop-btn">
									<a href="<?php bloginfo('url'); ?>/full">
										<img src="<?php bloginfo('template_url'); ?>/img/top/top_box_btn3.jpg" alt="top"/>
									</a>
								</div>    
							</div>							        
						</div><!-- end maintop-col -->		
					</div><!-- end maintop-row -->
				</div><!-- end maintop-group -->
			</section><!-- end maintop -->