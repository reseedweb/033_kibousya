<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- primary-row -->
		<?php get_template_part('part','breadcrumb'); ?>	 
		<h1 class="basic-top-title">	
			よくあるご質問
		</h1>
		<p>お客様からオリジナル紙袋制作Only Oneに寄せられたご質問についての回答を掲載しております。</p>
		<p>掲載事項以外の疑問点やお悩みなどがございましたら、メールフォームやお電話でお気軽にお問い合わせください。</p>
		<div class="primary-row clearfix"><!-- primary-row -->		
			<div class="faq-content-row clearfix">
				<div class="question-bg">
					紙袋の種類はどれくらいありますか？
				</div><!-- end question-bg -->
				<div class="answer-bg">
					<div class="answer">
						<p>素材、色、厚さ、柄など多種多様に用意しております。</p>
					</div>
				</div><!-- end answer-bg -->	
			</div><!-- end faq-content-row -->
		</div><!-- end primary-row -->
	</div><!-- end primary-row -->

	<div class="primary-row clearfix"><!-- primary-row -->
		<div class="faq-content-row clearfix">
			<div class="question-bg">
				キラキラした金や銀色などの色でも印刷できますか？
			</div><!-- end question-bg -->
			<div class="answer-bg">
				<div class="answer">
					<p>通常、箔押し印刷となります。料金等は別途お問合せください。</p>
				</div>
			</div><!-- end answer-bg -->	
		</div><!-- end faq-content-row -->
	</div><!-- end primary-row -->	
	
	<div class="primary-row clearfix"><!-- primary-row -->
		<div class="faq-content-row clearfix">
			<div class="question-bg">
				何色までプリントできますか？
			</div><!-- end question-bg -->
			<div class="answer-bg">
				<div class="answer">
					<p>色の数は何色でも可能です。ただし、１色印刷、２色印刷、４色印刷（フルカラー）では、印刷代が変わってまいります。</p>
				</div>
			</div><!-- end answer-bg -->	
		</div><!-- end faq-content-row -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- primary-row -->
		<div class="faq-content-row clearfix">
			<div class="question-bg">
				印刷の範囲はどこまで可能ですか？
			</div><!-- end question-bg -->
			<div class="answer-bg">
				<div class="answer">
					<p>当社では、バッグの前面に印刷が可能です。例えば、バッグの前後とマチにわたるようなデザインのご要望から、内面もカラフルに見せたいというご要望まであらゆるケースに対応可能です。</p>
				</div>
			</div><!-- end answer-bg -->	
		</div><!-- end faq-content-row -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- primary-row -->
		<div class="faq-content-row clearfix">
			<div class="question-bg">
				他の塗装業者との違いは？
			</div><!-- end question-bg -->
			<div class="answer-bg">
				<div class="answer">
					<p>
						自社で供給した高品質な塗料を使い、資格を取得した職人が施工致しますので、低価格・高品質・安心施工が可能です。また、自社の施工手順をしっかり守って作業致しますので、職人によって品質が違うということがなく、最高の仕上がりをご提供することが出来ます。
					</p>
				</div>
			</div><!-- end answer-bg -->	
		</div><!-- end faq-content-row -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- primary-row -->
		<div class="faq-content-row clearfix">
			<div class="question-bg">
				裏表で異なったデザインの紙袋を作りたいのですが…
			</div><!-- end question-bg -->
			<div class="answer-bg">
				<div class="answer">
					<p>裏表の異なる紙袋・手提げ袋のデザインのご依頼もお任せください。しかしながら、異なったふたつのデザインを使用する場合は、別途費用が発生いたしますのでご了承ください。</p>
				</div>
			</div><!-- end answer-bg -->	
		</div><!-- end faq-content-row -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- primary-row -->
		<div class="faq-content-row clearfix">
			<div class="question-bg">
				バッグのデザインを提出する際の注意事項は？
			</div><!-- end question-bg -->
			<div class="answer-bg">
				<div class="answer">
					<p>デザインはデジタルデータ(「ai」「eps」「psd」のいずれか)での入稿をお願いしております。手書きなどのデザインの場合は、一度デジタルデータに変換したものをご用意ください。</p>
				</div>
			</div><!-- end answer-bg -->	
		</div><!-- end faq-content-row -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- primary-row -->
		<div class="faq-content-row clearfix">
			<div class="question-bg">
				具体的なイメージがないのですが、大丈夫ですか？
			</div><!-- end question-bg -->
			<div class="answer-bg">
				<div class="answer">
					<p>事前にデザインにあたってのヒアリングをさせて頂きます。頂いた情報を元に制作させて頂きますので「こんな感じのもの」といった具体的イメージが無くても問題ございません。</p>
				</div>
			</div><!-- end answer-bg -->	
		</div><!-- end faq-content-row -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- primary-row -->
		<div class="faq-content-row clearfix">
			<div class="question-bg">
				フルオーダーは素人には難しそうなんだけど・・・。
			</div><!-- end question-bg -->
			<div class="answer-bg">
				<div class="answer">
					<p>今まで制作したサンプル等をご覧いただきながら、専門のスタッフが詳しく丁寧に対応しますので安心してお任せ下さい。</p>
				</div>
			</div><!-- end answer-bg -->	
		</div><!-- end faq-content-row -->
	</div><!-- end primary-row -->
	<?php get_template_part('part','contact'); ?>	
<?php get_footer(); ?>