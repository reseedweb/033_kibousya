
                    </main><!-- end primary -->
                    <aside class="sidebar"><!-- begin sidebar -->
                        <?php if(is_page('blog') || is_category() || is_single()) : ?>
                            <?php
                            $queried_object = get_queried_object();                                
                            $sidebar_part = 'blog';
                            if(is_tax() || is_archive()){                                    
                                $sidebar_part = '';
                            }                               

                            if($queried_object->post_type != 'post' && $queried_object->post_type != 'page'){
                                $sidebar_part = '';
                            }   
                                    
                            if($queried_object->taxonomy == 'category'){                                    
                                $sidebar_part = 'blog';
                            }                 
                            ?>
                            <?php get_template_part('sidebar',$sidebar_part); ?>  
                        <?php else: ?>
                            <?php get_template_part('sidebar'); ?>  
                        <?php endif; ?>                                    
                    </aside>                            
                </div><!-- end wrapper -->            
            </section><!-- end content -->

            <footer><!-- begin footer -->
				<div class="footer-content1 wrapper clearfix">            
					<div class="footer-logo">
						<a href="<?php bloginfo('url'); ?>/">
							<img alt="top" src="<?php bloginfo('template_url'); ?>/img/common/footer_logo.jpg" />
						</a>
					</div>
					<div class="footer-tel">
						<img alt="top" src="<?php bloginfo('template_url'); ?>/img/common/footer_tel.jpg" />
					</div>
					<div class="footer-con">
						<a href="<?php bloginfo('url'); ?>/contact">
							<img alt="top" src="<?php bloginfo('template_url'); ?>/img/common/footer_con.jpg" />
						</a> 
					</div>
				</div><!-- ./footer-content1 -->	
				<div class="footer-content2 wrapper clearfix">
					<div class="footer-info clearfix">
						<p>〒533-0005</p>
						<p class="footer-space">大阪府大阪市東淀川区 瑞光2丁目13-31</p>
						<p class="footer-space">TEL :06-6321-8111／FAX :06-6329-4893</p>
						<p class="footer-copyright">Copyright©2015 紙袋制作Only One All Rights Reserved.</p>
					</div>
					<div class="footer-navi clearfix">
						<ul>
							<li>
								<i class="fa fa-play"></i>
								<a href="<?php bloginfo('url'); ?>/">トップページ</a>
							</li>
                            <li>
								<i class="fa fa-play"></i>
								<a href="<?php bloginfo('url'); ?>/basic">ベーシック</a>
							</li>
							<li>
								<i class="fa fa-play"></i>
								<a href="<?php bloginfo('url'); ?>/semi">セミオーダー</a>
							</li>
							<li>
								<i class="fa fa-play"></i>
								<a href="<?php bloginfo('url'); ?>/full">フルオーダー</a>
							</li>							
						</ul>
					
						<ul>
							<li>
								<i class="fa fa-play"></i>
								<a href="<?php bloginfo('url'); ?>/about">初めての方へ</a>
							</li>
                            <li>
								<i class="fa fa-play"></i>
								<a href="<?php bloginfo('url'); ?>/flow">ご注文の流れ</a>
							</li>
							<li>
								<i class="fa fa-play"></i>
								<a href="<?php bloginfo('url'); ?>/price">参考価格例</a>
							</li>
							<li>
								<i class="fa fa-play"></i>
								<a href="<?php bloginfo('url'); ?>/factory">工場紹介</a>
							</li>							
						</ul>
						
						<ul>
							<li>
								<i class="fa fa-play"></i>
								<a href="<?php bloginfo('url'); ?>/blog">ブログ</a>
							</li>
                            <li>
								<i class="fa fa-play"></i>
								<a href="<?php bloginfo('url'); ?>/company">会社案内</a>
							</li>
							<li>
								<i class="fa fa-play"></i>
								<a href="<?php bloginfo('url'); ?>/draft">データ入稿について</a>
							</li>
							<li>
								<i class="fa fa-play"></i>
								<a href="<?php bloginfo('url'); ?>/faq">よくあるご質問</a>
							</li>							
						</ul>
						
						<ul>
							<li>
								<i class="fa fa-play"></i>
								<a href="<?php bloginfo('url'); ?>/privacy">プライバシーポリシー</a>
							</li>
                            <li>
								<i class="fa fa-play"></i>
								<a href="<?php bloginfo('url'); ?>/contact">お問い合わせ</a>
							</li>														
						</ul>

					</div>					
				</div><!-- ./footer-content2 -->
            </footer><!-- end footer -->            

        </div><!-- end wrapper -->
        <?php wp_footer(); ?>
        <!-- js plugins -->
        <script type="text/javascript">

            jQuery(document).ready(function(){
                // dynamic gNavi
                var gNavi_item_count = $('#gNavi .dynamic > li').length;
                if(gNavi_item_count > 0){
                    var gNavi_item_length = $('#gNavi .dynamic').width() / parseInt(gNavi_item_count);                    
                    $('#gNavi .dynamic > li').css('width', Math.floor( gNavi_item_length ) + 'px' )
                }               
                // bxslider
                jQuery('.bxslider').bxSlider({					
                    pagerCustom: '#bx-pager',                
					pause: 3000,
					speed: 1000,
					controls: false,
					auto: true,
					autoControls: false,
                });                
            }); 
        </script>                
    </body>
</html>