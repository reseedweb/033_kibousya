<div class="primary-row clearfix">    
    <div class="top-title clearfix">
		<h2 class="h2-title">新着情報</h2>    
		<h2 class="top-title-en">Information</h2>		
	</div>	
    <div id="part-blog">
        <div class="part-blog-content">
            <?php            
                $loop = new WP_Query('showposts=5&orderby=ID&order=DESC');
                if($loop->have_posts()):
            ?>
            
            <?php while($loop->have_posts()): $loop->the_post(); ?>
                <div class="item">                
                    <div class="date"><?php the_time('Y/m/d'); ?></div>
                    <div class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>                        
                </div>
            <?php endwhile; wp_reset_postdata();?>                        
            <?php endif; ?>                    
        </div>        
    </div>   
</div>