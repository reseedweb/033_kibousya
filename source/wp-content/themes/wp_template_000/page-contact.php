<?php get_header(); ?>	
	<?php get_template_part('part','breadcrumb'); ?>	
	<div class="main-content clearfix"><!-- begin main-content -->
		<h1 class="basic-top-title">	
			お問い合わせ
		</h1><!-- ./basic-top-title -->
		<p>お電話やフォームからお問い合わせ・お見積もりを承っております。お気軽にお問い合わせください。</p>		
		<div class="primary-row clearfix">
			<h2 class="factory-title">電話でのお問い合わせ</h2>	
			<div class="contact-text">
				<p>お電話でもお問い合わせ・お見積もりを承っております。お気軽にお問い合わせください。</p>
				<p>TEL：06-6321-8111 ／FAX：06-6329-4893</p>
			</div>
		</div>
		<div class="primary-row clearfix">
			<h2 class="factory-title">電話でのお問い合わせ</h2>
			<p>必要事項をご記入の上、送信ボタンを押して下さい。専門スタッフがメールを確認後、即日対応させて頂きます。</p>
			<p>(営業時間終了後の場合はご返信は翌営業日になります。ご了承ください)</p>
			<div class="contact-form">			
				<?php echo do_shortcode('[contact-form-7 id="248" title="無題"]') ?>
				<script src="http://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/ajaxzip3.js" charset="UTF-8"></script>
				<script type="text/javascript">
					$(document).ready(function(){
						$('#zip').change(function(){					
							//AjaxZip3.JSONDATA = "https://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/zipdata";
							AjaxZip3.zip2addr(this,'','pref','addr1','addr2');
						});
					});
				</script>			
			</div>
		</div>
	</div>
	<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>