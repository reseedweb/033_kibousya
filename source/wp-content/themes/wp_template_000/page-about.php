<?php get_header(); ?>	
	<?php get_template_part('part','breadcrumb'); ?>	
	<div class="main-content clearfix"><!-- begin main-content -->
		<h1 class="about-top-title">	
			初めての方へ
		</h1><!-- ./about-top-title -->
	</div>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		 <div class="top-title clearfix">
			<h2 class="h2-title">初めての方へのご案内</h2>    
			<h2 class="top-title-en">Beginner’s Guide</h2>		
		</div>	
		
		<div class="about-content-info1 clearfix">
			<div class="about-text">
				<h3 class="about-info1-title">
					オリジナル紙袋を制作されたいお客様へ！
				</h3>
				<p>オリジナル紙袋をどうやって制作したらいいか分からない…</p>
				<p>そんなお客様のオリジナル紙袋制作をサポートいたします！</p>
			</div>			
		</div>
				
		<div class="primary-row clearfix">			
			<h3 class="about-title clearfix">もっと安く作りたい！</h3>
			<div class="message-right message-220 clearfix">
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/about_image01.jpg" alt="about" />
				</div><!-- ./image -->
				<div class="text">
					<h4 class="msg220-title">
						オリジナル紙袋を制作するのに一体いくらかかるの？					
					</h4><!-- ./msg200-title -->
					<div class="msg220-text">
						箔押し・名入れ印刷で紙袋を制作して頂くことにより、オリジナル紙袋制作の価格を抑えることができます。以下の仕様項目からそれぞれ仕様を選んで頂いた後、ベーシック紙袋のお見積りフォームよりご依頼ください。
					</div><!-- ./msg200-text -->
				</div><!-- ./text -->
			</div><!-- end message-220 -->
		</div><!-- ./primary-row -->		
		
		<div class="primary-row clearfix">			
			<h3 class="about-title clearfix">急ぎの案件で困っている…</h3>
			<div class="message-right message-220 clearfix">
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/about_image02.jpg" alt="about" />
				</div><!-- ./image -->
				<div class="text">
					<h4 class="msg220-title">
						イベントの日までに欲しいんだけど間に合うの？					
					</h4><!-- ./msg220-title -->
					<div class="msg220-text">
						テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
					</div><!-- ./msg220-text -->
				</div><!-- ./text -->
			</div><!-- end message-220 -->
		</div><!-- ./primary-row -->
		
		<div class="primary-row clearfix">			
			<h3 class="about-title clearfix">様々な要望に応えてほしい！</h3>
			<div class="message-right message-220 clearfix">
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/about_image03.jpg" alt="about" />
				</div><!-- ./image -->
				<div class="text">
					<h4 class="msg220-title">
						作りたい紙袋にかなりこだわりがあるけど大丈夫？					
					</h4><!-- ./msg220-title -->
					<div class="msg220-text">
						テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
					</div><!-- ./msg220-text -->
				</div><!-- ./text -->
			</div><!-- end message-msg220 -->
		</div><!-- ./primary-row -->	
		
		<div class="primary-row clearfix">			
			<h3 class="about-title clearfix">入稿方法がわからない…</h3>
			<div class="message-right message-220 clearfix">
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/about_image04.jpg" alt="about" />
				</div><!-- ./image -->
				<div class="text">
					<h4 class="msg220-title">
						データ入稿って難しそうだけどどうやるの？					
					</h4><!-- ./msg220-title -->
					<div class="msg220-text">
						テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
					</div><!-- ./msg220-text -->
				</div><!-- ./text -->
			</div><!-- end message-220 -->
		</div><!-- ./primary-row -->					
	</div><!-- end primary-row -->	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		 <div class="top-title clearfix">
			<h2 class="h2-title">紙袋制作サイトOnly Oneが選ばれる理由</h2>    
			<h2 class="top-title-en">Why Choose</h2>		
		</div>			
		<div class="about-content-choice clearfix">
			<img src="<?php bloginfo('template_url'); ?>/img/content/about_choice01.jpg" alt="about" />
			<h3 class="about-choice-title">オリジナル紙袋が1枚81円〜　最短1週間で制作できる！</h3>
			<div class="about-choice-text">テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</div>	
		</div><!-- about-content-choice -->
		
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<div class="about-content-choice clearfix">
				<img src="<?php bloginfo('template_url'); ?>/img/content/about_choice02.jpg" alt="about" />
				<h3 class="about-choice-title">ブライダル紙袋ならお任せ　ワンランク上のクオリティ！</h3>
				<div class="about-choice-text">テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</div>	
			</div><!-- about-content-choice -->
		</div><!-- ./primary-row -->
		
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<div class="about-content-choice clearfix">
				<img src="<?php bloginfo('template_url'); ?>/img/content/about_choice03.jpg" alt="about" />
				<h3 class="about-choice-title">自社工場ならではの対応力　制作実績は10,000件以上！</h3>
				<div class="about-choice-text">テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</div>	
			</div><!-- about-content-choice -->
		</div><!-- ./primary-row -->
		
		<div class="primary-row clearfix"><!-- begin primary-row -->    
			<div class="top-content-info4 clearfix">
				<div class="info4-text1">
					オリジナル<br />
					ブライダル紙袋
				</div>		
				<div class="info4-text2">
					一生に一度の記念に♪
				</div>
				<div class="info4-text3">
					<span class="info4-icon">●</span>自分だけのオリジナルデザインが作れます！<br />
					<span class="info4-icon">●</span>高級感のあるワンランク上の作品が作れます！
				</div>
				<div class="info4-btn">
					<a href="<?php bloginfo('url'); ?>/bridal">
						<img src="<?php bloginfo('template_url'); ?>/img/content/top_bridal_btn.jpg" alt="top" />
					</a>
				</div>
			</div><!-- end top-content-info4 -->
		</div><!-- end primary-row -->	
		
	</div><!-- end primary-row -->
	<?php get_template_part('part','contact'); ?>	
<?php get_footer(); ?>